#version 410

in vec2 texCoord;
in vec2 noiseCoord;

in vec3 currPos;

in vec3 terrainNormal;

out vec4 fragColor;

uniform sampler2D sandSampler;
uniform sampler2D grassSampler;
uniform sampler2D rockSampler;
uniform sampler2D snowSampler;
uniform sampler2D noiseSampler;

struct Materials{
	vec4 Sand;
	vec4 Grass;
	vec4 Rock;
	vec4 Snow;
};

uniform Materials materials;

struct Light{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
	vec3 Direction;
};

uniform Light light;

//Metodos para calcular a luz
vec4 getDiffuseColor();
vec4 getAmbientColor();

//Metodos para realizar o blend das texturas
float getPercentage(sampler2D sampler, vec2 coord);
vec4 blendTextures(sampler2D sampler1, sampler2D sampler2, vec2 texCoord, sampler2D noise, vec2 noiseCoord, vec4 material1, vec4 material2);
vec4 addMaterial(vec4 fragColor, vec4 material);

void main() {
	float noiseLevel = getPercentage(noiseSampler, noiseCoord);
	float slope = 1-normalize(terrainNormal).z;
	//Armazena a altura em que o mapa foi criado
	//Pode ser usado como offset para gerar certos tipos de terrenos, conforme desejado
	float mapOffset = 0.0f;
	float height = currPos.z - mapOffset;
	
	if(height < 2.0f && slope < 0.1f){
		if(noiseLevel > 20.0f && noiseLevel < 45.0f){
			//Criamos uma imperfeição
			fragColor = blendTextures(sandSampler, rockSampler, texCoord, noiseSampler, noiseCoord, materials.Sand, materials.Rock);
		}
		else{
			fragColor = texture(sandSampler, texCoord.st) * materials.Sand;
		}
		
	}
	else{
		if(slope > 0.1f){
			if(noiseLevel > 40.0f && noiseLevel < 43.0f && height < 15.0f){
				//Criamos uma imperfeição com grama
				//fragColor = blendTextures(rockSampler, grassSampler, texCoord, noiseSampler, noiseCoord, materials.Rock, materials.Grass);
				fragColor = texture(grassSampler, texCoord.st) * materials.Grass * 0.6f;
			}
			else{
				fragColor = texture(rockSampler, texCoord.st) * materials.Rock;
			}
		}
		else{
			if(height < 15.0f){
				if(noiseLevel > 40.0f && noiseLevel < 44.0f){
					//Criamos uma imperfeição
					fragColor = texture(sandSampler, texCoord.st) * materials.Sand;
					//fragColor = blendTextures(grassSampler, sandSampler, texCoord, noiseSampler, noiseCoord, materials.Grass, materials.Sand);
				}
				else{
					fragColor = texture(grassSampler, texCoord.st) * materials.Grass;
				}
			}
			else if(height > 15.0f && height < 20.0f){
					fragColor = texture(rockSampler, texCoord.st) * materials.Rock;
			}
			else{
				if(noiseLevel > 20.0f && noiseLevel < 45.0f){
					//Criamos uma imperfeição
					fragColor = blendTextures(snowSampler, rockSampler, texCoord, noiseSampler, noiseCoord, materials.Snow, materials.Rock);
				}
				else{
					fragColor = texture(snowSampler, texCoord.st) * materials.Snow;
				}
			}
		}
	}
	fragColor = fragColor * (getAmbientColor() + getDiffuseColor());
}

vec4 getDiffuseColor(){
	float diffuseFactor = dot(normalize(terrainNormal), -light.Direction);

	vec4 diffuseColor;

	if (diffuseFactor > 0) {
		diffuseColor = vec4(light.Color, 1.0f) * light.DiffuseIntensity * diffuseFactor;
	}
	else {
		diffuseColor = vec4(0, 0, 0, 0);
	}
	return diffuseColor;
}

vec4 getAmbientColor(){
	return (vec4(light.Color, 1.0f) * light.AmbientIntensity);
}

float getPercentage(sampler2D sampler, vec2 coord){
	float percentage;
	
	percentage = texture(sampler, coord.st).x;
	percentage += texture(sampler, coord.st).y;
	percentage += texture(sampler, coord.st).z;
	percentage = percentage/3;
	percentage = percentage*100;
	
	return percentage;
}

vec4 blendTextures(sampler2D baseTex, sampler2D coverTex, vec2 texCoord, sampler2D noise, vec2 noiseCoord, vec4 baseMat, vec4 coverMat){
	vec4 blendFrag;
	float percentage = getPercentage(noise, noiseCoord);
	percentage = percentage/100.0f;
	
	blendFrag =  (1.0f - percentage) * texture(baseTex, texCoord.st) * baseMat;	
	blendFrag +=  percentage * texture(coverTex, texCoord.st) * coverMat;
	
	return blendFrag;
}

vec4 addMaterial(vec4 fragColor, vec4 material){
	return fragColor * material;
}