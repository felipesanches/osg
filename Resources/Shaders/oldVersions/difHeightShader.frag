#version 410

in vec2 texCoord;

in vec3 currPos;

in vec3 terrainNormal;

out vec4 fragColor;

uniform sampler2D sandSampler;
uniform sampler2D grassSampler;
uniform sampler2D rockSampler;
uniform sampler2D snowSampler;
uniform sampler2D noiseSampler;

struct Materials{
	vec4 Sand;
	vec4 Grass;
	vec4 Rock;
	vec4 Snow;
};

uniform Materials materials;

struct Light{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
	vec3 Direction;
};

uniform Light light;

//Metodos para calcular a luz
vec4 getDiffuseColor();
vec4 getAmbientColor();

void main() {
	if(currPos.z < 1.0f){
		fragColor = texture(sandSampler, texCoord.st) * materials.Sand;
	}													
	else if(currPos.z < 5.0f){							
	    fragColor = texture(grassSampler, texCoord.st) * materials.Grass * texture(noiseSampler, texCoord.st);;
	}													
	else if(currPos.z < 25.0f){							
		fragColor = texture(rockSampler, texCoord.st) * materials.Rock;
	}													
	else{												
		fragColor = texture(snowSampler, texCoord.st) * materials.Snow;
	}
	
	fragColor = fragColor * (getAmbientColor() + getDiffuseColor());
}

vec4 getDiffuseColor(){
	float diffuseFactor = dot(normalize(terrainNormal), -light.Direction);

	vec4 diffuseColor;

	if (diffuseFactor > 0) {
		diffuseColor = vec4(light.Color, 1.0f) * light.DiffuseIntensity * diffuseFactor;
	}
	else {
		diffuseColor = vec4(0, 0, 0, 0);
	}
	return diffuseColor;
}

vec4 getAmbientColor(){
	return (vec4(light.Color, 1.0f) * light.AmbientIntensity);
}