#version 410

in vec2 texCoord;
in vec2 noiseCoord;

in vec3 currPos;

in vec3 terrainNormal;

out vec4 fragColor;

uniform sampler2D sandSampler;
uniform sampler2D grassSampler;
uniform sampler2D rockSampler;
uniform sampler2D snowSampler;
uniform sampler2D noiseSampler;

struct Materials{
	vec4 Sand;
	vec4 Grass;
	vec4 Rock;
	vec4 Snow;
};

uniform Materials materials;

struct Light{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
	vec3 Direction;
};

uniform Light light;

//Metodos para calcular a luz
vec4 getDiffuseColor();
vec4 getAmbientColor();
//Metodos para realizar o blend das texturas
float getPercentage(sampler2D sampler, vec2 coord);
vec4 blendTextures(sampler2D sampler1, sampler2D sampler2, vec2 texCoord, sampler2D noise, vec2 noiseCoord, vec4 material1, vec4 material2);
vec4 addMaterial(vec4 fragColor, vec4 material);

void main() {
	if(currPos.z < 2.0f){
		fragColor = texture(sandSampler, texCoord.st) * materials.Sand;
	}
	else if(currPos.z > 2.0f && currPos.z < 5.0f){
		fragColor = blendTextures(sandSampler, grassSampler, texCoord, noiseSampler,
									noiseCoord, materials.Sand, materials.Grass);
	}
	else if(currPos.z > 5.0f && currPos.z < 7.0f){							
	    fragColor = texture(grassSampler, texCoord.st) * materials.Grass;
	}												
	else if(currPos.z > 7.0f && currPos.z < 10.0f){
		fragColor = blendTextures(rockSampler, grassSampler, texCoord, noiseSampler,
									noiseCoord, materials.Rock, materials.Grass);
	}
	else if(currPos.z > 10.0f && currPos.z < 20.0f){							
		fragColor = texture(rockSampler, texCoord.st) * materials.Rock;
	}												
	else if(currPos.z > 20.0f && currPos.z < 25.0f){
		fragColor = blendTextures(rockSampler, snowSampler, texCoord, noiseSampler, noiseCoord,
									materials.Rock, materials.Snow);
	}
	else{												
		fragColor = texture(snowSampler, texCoord.st) * materials.Snow;
	}
	
	fragColor = fragColor * (getAmbientColor() + getDiffuseColor());
}

vec4 getDiffuseColor(){
	float diffuseFactor = dot(normalize(terrainNormal), -light.Direction);

	vec4 diffuseColor;

	if (diffuseFactor > 0) {
		diffuseColor = vec4(light.Color, 1.0f) * light.DiffuseIntensity * diffuseFactor;
	}
	else {
		diffuseColor = vec4(0, 0, 0, 0);
	}
	return diffuseColor;
}

vec4 getAmbientColor(){
	return (vec4(light.Color, 1.0f) * light.AmbientIntensity);
}

float getPercentage(sampler2D sampler, vec2 coord){
	float percentage;
	
	percentage = texture(sampler, coord.st).x;
	percentage += texture(sampler, coord.st).y;
	percentage += texture(sampler, coord.st).z;
	percentage = percentage/3;
	
	return percentage;
}

vec4 blendTextures(sampler2D baseTex, sampler2D coverTex, vec2 texCoord, sampler2D noise, vec2 noiseCoord, vec4 baseMat, vec4 coverMat){
	vec4 blendFrag;
	float percentage = getPercentage(noise, noiseCoord);
	
	if(percentage > 0.5f){
		blendFrag = texture(baseTex, texCoord.st);
		blendFrag = addMaterial(blendFrag, baseMat);
	}
	/*
	else if(percentage > 0.25f && percentage < 0.75f){
		blendFrag = texture(baseTex, noiseCoord.st) * percentage;
		blendFrag = addMaterial(blendFrag, baseMat);
		blendFrag += texture(coverTex, noiseCoord.st) * (1 - percentage);
		blendFrag = addMaterial(blendFrag, coverMat); 
	}
	*/
	else{
		blendFrag = texture(coverTex, texCoord.st);
		blendFrag = addMaterial(blendFrag, coverMat);
	}
	
	//blendFrag =  (1.0f - percentage) * texture(baseTex, texCoord.st) * baseMat;	
	//blendFrag +=  percentage * texture(coverTex, texCoord.st) * coverMat;
					
	return blendFrag;
}

vec4 addMaterial(vec4 fragColor, vec4 material){
	return fragColor * material;
}