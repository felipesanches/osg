#version 410

in vec2 texCoord;
in vec3 terrainNormal;

out vec4 fragColor;

uniform sampler2D grassSampler;
uniform vec3 lightColor;
uniform float lightAmbIntensity;
uniform vec3 lightDirection;
uniform float lightDiffuseIntensity;

//Metodos para calcular a luz
vec4 getDiffuseColor(vec3 normal, vec3 lightDir, float difIntensity, vec3 lightColor);
vec4 getAmbientColor(vec3 lightColor, float ambIntensity);

void main() {
		fragColor = texture(grassSampler, texCoord.st) * 
						(getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
}

vec4 getDiffuseColor(vec3 normal, vec3 lightDir, float difIntensity, vec3 lightColor){
	float diffuseFactor = dot(normalize(normal), -lightDir);

	vec4 diffuseColor;

	if (diffuseFactor > 0) {
		diffuseColor = vec4(lightColor, 1.0f) * difIntensity * diffuseFactor;
	}
	else {
		diffuseColor = vec4(0, 0, 0, 0);
	}
	return diffuseColor;
}

vec4 getAmbientColor(vec3 lightColor, float ambIntensity){
	return vec4(lightColor, 1.0f) * ambIntensity;
}