#version 410

in vec3 position;
in vec3 normal;

out vec3 currPos;
out vec2 texCoord;
out vec2 noiseCoord;
out vec3 terrainNormal;

uniform mat4 worldMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec3 firstVertex;

// Metodos utilizados para informar qual a escala da textura
float getSamplerSSize();
float getSamplerTSize();

void main() {
	// Utilizando as matrizes, definimos a posicao do vertice
    gl_Position = projectionMatrix * viewMatrix * worldMatrix * vec4(position, 1.0);

    // Devemos definir a coordenada de textura, e para isso utilizamos o primeiro vértice
    float sCoord, tCoord;
    sCoord = (position.x - firstVertex.x) / getSamplerSSize();
    tCoord = (position.y - firstVertex.y) / getSamplerTSize();
    texCoord = vec2(sCoord, tCoord);
	
	// Devemos definir a coordenad de textura para o noise
	// Usada também para criar os "defeitos" no mapa
	sCoord = position.x / 50;
	tCoord = position.y / 50;
	noiseCoord = vec2(sCoord, tCoord);

	terrainNormal = (worldMatrix * vec4(normal, 0.0)).xyz;

	currPos = position;
}

float getSamplerSSize() {
	return 5;
}

float getSamplerTSize() {
	return 5;
}