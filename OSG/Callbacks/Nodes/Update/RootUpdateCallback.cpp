#include "RootUpdateCallback.h"
#include "../../../Utils/Utils.h"

#include <osg/Node>

using namespace osgTCC;

RootUpdateCallback::RootUpdateCallback(EventStatus* statusData) : osg::NodeCallback() {
	this->statusData = statusData;
}

void RootUpdateCallback::operator()(osg::Node *node, osg::NodeVisitor *nv) {
	if (statusData->dirty) {
		statusData->dirty = false;
		wireFrameMode(node, statusData->wireframe);
		lightingMode(node, statusData->lighting);
		textureMode(node, statusData->texture);
	}
	
	traverse(node, nv);
}