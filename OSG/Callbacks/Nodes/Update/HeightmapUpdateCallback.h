#ifndef HEIGHTMAPUPDATECALL_H
#define HEIGHTMAPUPDATECALL_H

#include <osg/NodeCallback>

#include "../../../UserDatas/EventStatus.h"

#include "../../../Utils/Utils.h"

#include "../../../Geodes/HeightField/HeightFieldGeode.h"

namespace osgTCC {
	class Factory;

	class HeightmapUpdateCallback : public osg::NodeCallback {
	public:
		HeightmapUpdateCallback(EventStatus *eventStatus, Factory* factory);
		virtual void operator()(osg::Node *node, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~HeightmapUpdateCallback() {}

		osg::ref_ptr<EventStatus> _eventStauts;
		Factory* _factory;
	};
}
#endif /* HEIGHTMAPUPDATECALL_H */