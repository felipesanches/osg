#ifndef __ROOTUPDATECALLBACK_H__
#define __ROOTUPDATECALLBACK_H__

#include <osg/NodeCallback>

#include "../../../UserDatas/EventStatus.h"

namespace osgTCC {
	class RootUpdateCallback : public osg::NodeCallback {
	public:
		RootUpdateCallback(EventStatus *statusData);
		virtual void operator()(osg::Node *node, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~RootUpdateCallback() {}

		osg::ref_ptr<EventStatus> statusData;
	};
}
#endif // !__ROOTUPDATECALLBACK_H__