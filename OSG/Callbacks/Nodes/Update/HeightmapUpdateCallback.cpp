#include "HeightmapUpdateCallback.h"
#include "../../../Utils/Factory.h"

using namespace osgTCC;

HeightmapUpdateCallback::HeightmapUpdateCallback(EventStatus *eventStatus, Factory* factory) : osg::NodeCallback() {
	_eventStauts = eventStatus;
	_factory = factory;
}

void HeightmapUpdateCallback::operator()(osg::Node *node, osg::NodeVisitor *nv) {
	HeightFieldGeode* heightField = dynamic_cast<HeightFieldGeode*>(node);
	
	if(_eventStauts->heightmapChanges == HM_BOTH || _eventStauts->heightmapChanges == HM_TEXTURE){
		//heightField->loadNewHeightMap("../Resources/HeightMaps/" + _eventStauts->heightmapName, 40.0, 1.0, 1.0,
		//								_eventStauts->aoe, 0.01f);
		heightField->loadNewHeightMap("../Resources/HeightMaps/" + _eventStauts->heightmapName);
	}
	if(_eventStauts->heightmapChanges == HM_BOTH || _eventStauts->heightmapChanges == HM_SHADER){
		// Verifica qual o tipo do shader
		std::size_t found;
		found = _eventStauts->fragShaderName.find(".frag");
		std::size_t found2;
		found2 = _eventStauts->vertShaderName.find(".vert");

		if(found == std::string::npos && found2 == std::string::npos){
			_eventStauts->fragShaderName = _eventStauts->fragShaderName + ".frag";
			_eventStauts->vertShaderName = _eventStauts->vertShaderName + ".vert";
			
		}
		_factory->setupShaders();
	}

	_eventStauts->heightmapChanges = HM_NONE;
	heightField->setRadius(_eventStauts->aoe);
	
	traverse(node, nv);
}