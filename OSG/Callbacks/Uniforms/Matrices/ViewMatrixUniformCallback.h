#ifndef __VIEWMATRIXUNIFORMCALLBACK_H__
#define __VIEWMATRIXUNIFORMCALLBACK_H__

#include <osg/Uniform>
#include <osg/Camera>

namespace osgTCC {
	class ViewMatrixUniformCallback : public osg::Uniform::Callback {
	public:
		ViewMatrixUniformCallback(osg::Camera *camera);
		virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~ViewMatrixUniformCallback() {}

		// A c�mera utilizada para obter a matriz de view
		osg::Camera* camera;
	};
}

#endif // !__VIEWMATRIXUNIFORMCALLBACK_H__