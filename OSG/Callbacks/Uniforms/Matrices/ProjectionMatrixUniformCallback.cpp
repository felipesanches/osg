#include "ProjectionMatrixUniformCallback.h"

using namespace osg;
using namespace osgTCC;

ProjectionMatrixUniformCallback::ProjectionMatrixUniformCallback(Camera* camera) : Uniform::Callback() {
	// Associamos a c�mera
	this->camera = camera;
}

void ProjectionMatrixUniformCallback::operator()(osg::Uniform* uniform, osg::NodeVisitor* nv) {
	// Atualizamos o valor da uniform
	uniform->set(camera->getProjectionMatrix());
}