#ifndef __PROJECTIONMATRIXUNIFORMCALLBACK_H__
#define __PROJECTIONMATRIXUNIFORMCALLBACK_H__

#include <osg/Uniform>
#include <osg/Camera>

namespace osgTCC {
	class ProjectionMatrixUniformCallback : public osg::Uniform::Callback {
	public:
		ProjectionMatrixUniformCallback(osg::Camera *camera);
		virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~ProjectionMatrixUniformCallback() {}

		// A c�mera utilizada para obter a matriz de projection
		osg::Camera* camera;
	};
}

#endif // !__PROJECTIONMATRIXUNIFORMCALLBACK_H__