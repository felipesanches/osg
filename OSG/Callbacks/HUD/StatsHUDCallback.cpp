#include "StatsHUDCallback.h"
#include "../../HUD/Others/StatsHUD.h"

using namespace osgTCC;

StatsHUDCallback::StatsHUDCallback(EventStatus *eventStatus) {
	_eventStatus = eventStatus;
}

void StatsHUDCallback::operator()(osg::Node *node, osg::NodeVisitor *nv) {
	// Obtemos o StatsHUD
	StatsHUD *statsHUD = dynamic_cast<StatsHUD*>(node);
	
	// Checamos se ele est� sendo mostrado ou n�o
	if (statsHUD->isShowing()) {
		statsHUD->setNodeMask(0xFFFFFFFF);
		statsHUD->updateFPS();
	} else {
		statsHUD->setNodeMask(0xFFFFFFFE);
	}
	
	traverse(node, nv);
}