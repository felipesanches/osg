#ifndef INPUTHUDCALLBACK_H
#define INPUTHUDCALLBACK_H

#include <osg/NodeCallback>

#include "../../UserDatas/EventStatus.h"
#include "../../HUD/Interfaces/InputHUD.h"

namespace osgTCC {
	class InputHUDCallback : public osg::NodeCallback {
	public:
		InputHUDCallback(EventStatus *eventStatus);
		virtual void InputHUDCallback::operator()(osg::Node *node, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~InputHUDCallback() {}

		osg::ref_ptr<EventStatus> _eventStatus;
	};
}
#endif /* INPUTHUDCALLBACK_H */