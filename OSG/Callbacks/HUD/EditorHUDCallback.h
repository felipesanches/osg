#ifndef EDITORHUDCALLBACK_H
#define EDITORHUDCALLBACK_H

#include <osg/NodeCallback>

#include "../../UserDatas/EventStatus.h"
#include "../../HUD/Interfaces/editorHUD.h"
#include "../../Camera/FreeCameraManipulator.h"

namespace osgTCC {
	class EditorHUDCallback : public osg::NodeCallback {
	public:
		EditorHUDCallback(EventStatus *eventStatus);
		virtual void operator()(osg::Node *node, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~EditorHUDCallback() {}

		osg::ref_ptr<EventStatus> _eventStatus;
		//osg::ref_ptr<FreeCameraManipulator> _camManipulator;
	};
}
#endif /* EDITORHUDCALLBACK_H */