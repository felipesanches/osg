#ifndef EVENTSTATUS_H
#define EVENTSTATUS_H

#include <osg/Referenced>
#include <iostream>

namespace osgTCC {
	enum cameraMoviment {
		CAMERA_FORWARD,
		CAMERA_BACKWARD,
		CAMERA_LEFT,
		CAMERA_RIGHT,
		CAMERA_FORWARD_LEFT,
		CAMERA_FORWARD_RIGHT,
		CAMERA_BACKWARD_LEFT,
		CAMERA_BACKWARD_RIGHT,
		CAMERA_UP,
		CAMERA_DOWN,
		CAMERA_NOP
	};
	enum hudStatus {
		HUD_OPEN,
		HUD_INUSE,
		HUD_NOP,
		HUD_CLOSE
	};
	enum AOE {
		INCREASE_AOE,
		DECREASE_AOE,
		NOP
	};
	enum heightMapChanges {
		HM_TEXTURE,
		HM_SHADER,
		HM_BOTH,
		HM_NONE
	};
	enum mouseFreeMode {
		MFM_ON,
		MFM_OFF,
		MFM_TRANSITION
	};
	class EventStatus : public osg::Referenced {
	public:
		// Informa se o modo wireframe est� selecionado.
		bool wireframe;
		// Informa se o modo lighting est� selecionado.
		bool lighting;
		// Informa se o modo texture est� selecionado.
		bool texture;
		// Informa se houve alguma mudan�a de estado.
		bool dirty;
		// Informa se o usuario est� usando a HUD
		bool cameraEnabled;
		// Informa o status da HUD do editor
		hudStatus editorHudStatus;
		// Informa o status da HUD de input
		hudStatus inputHudStatus;
		// Informa se o botao esquedo do mouse foi acionado
		bool mouseLeftB;
		// Informa a ultima posicao do mouse X
		int mouseX;
		// Informa a ultima posicao do mouse Y
		int mouseY;
		// Informa o tipo de movimento da camera
		cameraMoviment camMovimentDirection;
		// Informa se a tecla digitada ja foi lida ou n�o
		bool keyRead;
		// Informa o valor da ultima tecla precionada no teclado
		int keyboardKeyValue;
		// Informnda o width da janela principal
		float windowWidth;
		// Informa o height da janela principal
		float windowHeight;
		// Informa se ocorreu resize event
		bool resizeEvent;
		// Informa o nome do arquivo que contem o heightmap
		std::string heightmapName;
		// Informa o nome do arquivo que contem o fragment shader
		std::string fragShaderName;
		// Informa o nome do arquivo que contem o vertex shader
		std::string vertShaderName;
		// Informa se esta sendo solicitada a troca do heightmap
		heightMapChanges heightmapChanges;
		// Informa a area que as altera��es de altura ter� efeito
		float aoe;
		// Informa se a area de alteracoes aumentou/diminuiu ou nada.
		AOE AOEinfo;
		// Informa se o mouse foi movido pelo handler do osg
		// Ao se relizar o wrap do mouse o handler � chamado logo precisamos
		// de alguma forma para informar se aquela chama � por causa do wrap ou outro evento
		bool wrapMouseEvent;
		// Flag que liberar o mouse de sofrer o wrap. Permitindo assim a livre movimentacao desse
		mouseFreeMode freeMouseToggle;


		// Construtor que por default, desabilita apenas o modo wireframe.
		EventStatus(int windowWidth, int windowHeight);

	protected:
		// Destrutor privado
		~EventStatus() {}
	};
}
#endif //!EVENTSSTATUS_H