#include "EventStatus.h"

using namespace osgTCC;

EventStatus::EventStatus(int windowWidth, int windowHeight) : osg::Referenced() {
	dirty = true;
	wireframe = false;
	lighting = true;
	texture = true;
	editorHudStatus = HUD_CLOSE;
	cameraEnabled = true;
	inputHudStatus = HUD_CLOSE;
	mouseLeftB = false;
	camMovimentDirection = CAMERA_NOP;
	keyRead = true;
	this->windowHeight = windowHeight;
	this->windowWidth = windowWidth;
	resizeEvent = false;
	heightmapChanges = HM_NONE;
	AOEinfo = NOP;
	aoe = 40.0;
	wrapMouseEvent = false;
	freeMouseToggle = MFM_OFF;
	fragShaderName = "imperfection.frag";
	vertShaderName = "slopeVertex.vert";
	heightmapName = "real1.png";
}