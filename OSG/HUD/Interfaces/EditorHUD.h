#ifndef EDITORHUD_H
#define EDITORHUD_H

#include "../Basic/CircleHUD.h"

namespace osgTCC {
	class EditorHUD : public CircleHUD {
	public: 
		EditorHUD(float windowWidth, float windowHeight, float radius);
		void increaseRadius();
		void decreaseRadius();

	protected:
		~EditorHUD() {}

		float _radiusStep;
		float _radiusMax;
		float _radiusMin;

	};
}
#endif /* EDITORHUD_H */