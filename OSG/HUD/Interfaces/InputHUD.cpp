#include "InputHUD.h"

using namespace osgTCC;
using namespace osgGA;

InputHUD::InputHUD(float windowWidth, float windowHeight, const char* currVertexShaderName, const char* currFragShaderName, const char* currHeightmapName) : 
	RectangleHUD(windowWidth, windowHeight, windowWidth, windowHeight,
					osg::Vec2f(windowWidth/2, windowHeight/2), 0.8, INT_MAX-5, false, osg::Vec4(0.6, 0.6, 0.6, 0.8)) {

	_okButton = new Button(windowWidth, windowHeight, 50, 20, osg::Vec2f(60, 20), 1, INT_MAX - 5, true, osg::Vec4(1,1,1,1));
	_okButton->setButtonText(20, "OK", buttonTextPos::CENTER, osg::Vec4f(0,0.4,0,1));

	_inputPath = new InputBox(windowWidth, windowHeight, 300, 30, osg::Vec2f(30, 20), 1, INT_MAX - 5, true, osg::Vec4(1,1,1,1));
	_inputPath->setButtonText(25, currHeightmapName, buttonTextPos::LEFT, osg::Vec4f(0,0.4,0,1));

	_inputFragShader = new InputBox(windowWidth, windowHeight, 300, 30, osg::Vec2f(30, 40), 1, INT_MAX -5, true, osg::Vec4(1,1,1,1));
	_inputFragShader->setButtonText(25, currFragShaderName, buttonTextPos::LEFT, osg::Vec4f(0,0.4,0,1));

	_inputVertShader = new InputBox(windowWidth, windowHeight, 300, 30, osg::Vec2f(30, 50), 1, INT_MAX -5, true, osg::Vec4(1,1,1,1));
	_inputVertShader->setButtonText(25, currVertexShaderName, buttonTextPos::LEFT, osg::Vec4f(0,0.4,0,1));

	_pathCheck = new CheckBox(windowWidth, windowHeight, 20, osg::Vec2f(10,25), 1, INT_MAX - 5, true, "Incluir Heightfield",
					osg::Vec4(0,0.4,0,1), osg::Vec4(1,1,1,1));

	_shaderCheck = new CheckBox(windowWidth, windowHeight, 20, osg::Vec2f(10,55), 1, INT_MAX - 5, true, "Incluir Shader",
					osg::Vec4(0,0.4,0,1), osg::Vec4(1,1,1,1));

	_labelLoad = new Label(windowWidth, windowHeight, osg::Vec2f(50, 50), 1, INT_MAX - 2, true);
	_labelLoad->setLabelText(30, "", osg::Vec4f(1,0,0,1));

	_checkBoxComb = NONE;

	this->addChild(_okButton->getButtonHud());
	this->addChild(_inputPath->getButtonHud());
	this->addChild(_inputFragShader->getButtonHud());
	this->addChild(_inputVertShader->getButtonHud());
	this->addChild(_pathCheck->getButtonHud());
	this->addChild(_shaderCheck->getButtonHud());
	this->addChild(_labelLoad);
}

bool InputHUD::checkCollision(float x, float y) {
	if(_okButton->getCollision(x, y)){
		_inputPath->setWritingFlag(false);
		_inputFragShader->setWritingFlag(false);
		update();
		return true;
	}
	else if(_inputPath->getCollision(x, y)){
		if(!_inputPath->getWritingFlag()){
			_inputPath->setWritingFlag(true);
			_inputFragShader->setWritingFlag(false);
			_inputVertShader->setWritingFlag(false);
			_inputPath->setButtonText("");
			update();
			return false;
		}
	}
	else if(_inputFragShader->getCollision(x, y)){
		if(!_inputFragShader->getWritingFlag()){
			_inputFragShader->setWritingFlag(true);
			_inputPath->setWritingFlag(false);
			_inputVertShader->setWritingFlag(false);
			_inputFragShader->setButtonText("");
			update();
			return false;
		}
	}
	else if(_inputVertShader->getCollision(x, y)){
		if(!_inputVertShader->getWritingFlag()){
			_inputVertShader->setWritingFlag(true);
			_inputFragShader->setWritingFlag(false);
			_inputPath->setWritingFlag(false);
			_inputVertShader->setButtonText("");
			update();
			return false;
		}
	}
	else if(_pathCheck->getCollision(x, y)){
		_pathCheck->setButtonText();
		if(_pathCheck->getCheckBoxMark()){
			if(_checkBoxComb == NONE)
				_checkBoxComb = PATHONLY;
			else if(_checkBoxComb == SHADERONLY)
				_checkBoxComb = BOTH;
		}
		else{
			if(_checkBoxComb == PATHONLY)
				_checkBoxComb = NONE;
			else if(_checkBoxComb == BOTH)
				_checkBoxComb = SHADERONLY;
		}
		update();
		return false;
	}
	else if(_shaderCheck->getCollision(x, y)){
		_shaderCheck->setButtonText();
		if(_shaderCheck->getCheckBoxMark()){
			if(_checkBoxComb == NONE)
				_checkBoxComb = SHADERONLY;
			else if(_checkBoxComb == PATHONLY)
				_checkBoxComb = BOTH;
		}
		else{
			if(_checkBoxComb == SHADERONLY)
				_checkBoxComb = NONE;
			else if(_checkBoxComb == BOTH)
				_checkBoxComb = PATHONLY;
		}
		update();
		return false;
	}
	update();
	return false;
}

void InputHUD::update(char letter){
	if(_inputPath->getWritingFlag() == true){
		_inputPath->writeLetter(letter);
	}
	else if(_inputFragShader->getWritingFlag() == true){
		_inputFragShader->writeLetter(letter);
	}
	else if(_inputVertShader->getWritingFlag() == true){
		_inputVertShader->writeLetter(letter);
	}
	_inputPath->updateWritingFlag();
	_inputFragShader->updateWritingFlag();
	_inputVertShader->updateWritingFlag();
}

void InputHUD::update(){
	_inputPath->updateWritingFlag();
	_inputFragShader->updateWritingFlag();
	_inputVertShader->updateWritingFlag();
}

std::string InputHUD::getInputPath(){
	return _inputPath->getButtonText();
}

std::string InputHUD::getFragShaderPath(){
	return _inputFragShader->getButtonText();
}

std::string InputHUD::getVertShaderPath(){
	return _inputVertShader->getButtonText();
}

void InputHUD::setIsWriting(bool stop){
	_inputPath->setWritingFlag(stop);
	_inputFragShader->setWritingFlag(stop);
	_inputVertShader->setWritingFlag(stop);
}

void InputHUD::setLabel(){
	_labelLoad->setLabelText("Loading...");
}

void InputHUD::removeLabel(){
	_labelLoad->setLabelText("");
}

void InputHUD::resetCheckBoxes(){
	_checkBoxComb = NONE;
	_shaderCheck->clearMark();
	_pathCheck->clearMark();
}

int InputHUD::getCheckBoxesCombination(){
	return _checkBoxComb;
}

void InputHUD::resize(double windowWidth, double windowHeight){	
	float buttonWidth = _okButton->getButtonHud()->getHUDwidth();
	float buttonHeight = _okButton->getButtonHud()->getHUDheight();

	//reDraw(windowWidth, windowHeight, windowWidth, windowHeight, osg::Vec2f(windowWidth/2, windowHeight/2), false);

	_okButton->reDraw(windowWidth, windowHeight, buttonWidth, buttonHeight, osg::Vec2f(60, 20), true);

	buttonWidth = _inputPath->getButtonHud()->getHUDwidth();
	buttonHeight = _inputPath->getButtonHud()->getHUDheight();
	_inputPath->reDraw(windowWidth, windowHeight, buttonWidth, buttonHeight, osg::Vec2f(30, 20), true);

	buttonWidth = _inputFragShader->getButtonHud()->getHUDwidth();
	buttonHeight = _inputFragShader->getButtonHud()->getHUDheight();
	_inputFragShader->reDraw(windowWidth, windowHeight, buttonWidth, buttonHeight, osg::Vec2f(30, 40), true);

	buttonWidth = _inputVertShader->getButtonHud()->getHUDwidth();
	buttonHeight = _inputVertShader->getButtonHud()->getHUDheight();
	_inputVertShader->reDraw(windowWidth, windowHeight, buttonWidth, buttonHeight, osg::Vec2f(30, 50), true);

	buttonWidth = _pathCheck->getButtonHud()->getHUDwidth();
	buttonHeight = _pathCheck->getButtonHud()->getHUDheight();
	_pathCheck->reDraw(windowWidth, windowHeight, buttonWidth, buttonHeight, osg::Vec2f(10, 25), true);

	buttonWidth = _shaderCheck->getButtonHud()->getHUDwidth();
	buttonHeight = _shaderCheck->getButtonHud()->getHUDheight();
	_shaderCheck->reDraw(windowWidth, windowHeight, buttonWidth, buttonHeight, osg::Vec2f(10, 55), true);

	_labelLoad->reDraw(windowWidth, windowHeight, true);
}