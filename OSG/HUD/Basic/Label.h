#ifndef LABEL_H
#define LABEL_H

#include <osgText/Text>
#include <osgText/Font>
#include <osgText/String>

#include "../../HUD/Basic/RectangleHUD.h"

namespace osgTCC {
	class Label : public HudProjection {
	public:
		Label(int windowWidth, int windowHeight, osg::Vec2f pos, float alpha, bool isRelativePos);
		Label(int windowWidth, int windowHeight, osg::Vec2f pos, float alpha, int renderPriority, bool isRelativePos);
		void setLabelText(int fontSize, std::string buttonText, osg::Vec4f textColor);
		void setLabelText(std::string text);
		std::string getLabelText();
		void reDraw(double windowWidth, double windowHeight, bool isRelativePos);

	protected:
		// Destrutor protegido.
		virtual ~Label() {}

		void confAlphaStateSet(float alpha);
		void confPriorityStateSet(int renderPriority);
		void initialize(int windowWidth, int windowHeight, osg::Vec2f pos, float alpha, bool isRelativePos);

		osg::ref_ptr<osgText::Text> _labelText;
		float _windowWidth;
		float _windowHeight;
		osg::Vec2f _labelPos;

		virtual void Label::initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos);
		virtual void Label::initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos);
		virtual void Label::initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos);
	};
}
#endif /* LABEL_H */