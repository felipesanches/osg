#include "Label.h"

#include <iostream>

using namespace osg;
using namespace osgTCC;

Label::Label(int windowWidth, int windowHeight, osg::Vec2f pos, float alpha, bool isRelativePos) :
HudProjection(windowWidth, windowHeight, alpha, INT_MAX){

	initialize(windowWidth, windowHeight, pos, alpha, isRelativePos);
}

Label::Label(int windowWidth, int windowHeight, osg::Vec2f pos, float alpha, int renderPriority, bool isRelativePos) :
HudProjection(windowWidth, windowHeight, alpha, renderPriority){
	
	initialize(windowWidth, windowHeight, pos, alpha, isRelativePos);
	confPriorityStateSet(renderPriority);
}

void Label::initialize(int windowWidth, int windowHeight, Vec2f pos, float alpha, bool isRelativePos){

	_labelText = new osgText::Text();
	_labelText->setDataVariance(osg::Object::DYNAMIC);

	_windowWidth = windowWidth;
	_windowHeight = windowHeight;
	
	if(!isRelativePos)
		_labelPos = Vec2f(pos.x(), pos.y());
	else
		_labelPos = Vec2f(windowWidth*(pos.x()/100), windowHeight*(pos.y()/100));


	confAlphaStateSet(alpha);

	_HUDGeode->removeDrawable(_HUDBackground);
	_HUDGeode->addDrawable(_labelText);
}

void Label::confAlphaStateSet(float alpha){
	if(alpha == 1){
		_HUDGeode->getOrCreateStateSet()->setMode(GL_BLEND, StateAttribute::OFF | StateAttribute::OVERRIDE);
	}
	else{
		_HUDGeode->getOrCreateStateSet()->setMode(GL_BLEND, StateAttribute::ON | StateAttribute::OVERRIDE);
	}
	
}

void Label::confPriorityStateSet(int renderPriority){
	_HUDGeode->getOrCreateStateSet()->setRenderBinDetails(renderPriority + 1, "RenderBin");
}

void Label::setLabelText(int fontSize, std::string buttonText, osg::Vec4f textColor) {
	// Set up the parameters for the text we'll add to the HUD:
    _labelText->setCharacterSize(fontSize);
    _labelText->setFont("C:/WINDOWS/Fonts/impact.ttf");
    _labelText->setText(buttonText);
    _labelText->setAxisAlignment(osgText::Text::SCREEN);
    _labelText->setColor(textColor);
	_labelText->setPosition(osg::Vec3(_labelPos.x(), _labelPos.y(), 0));
}

void Label::setLabelText(std::string text) {
	_labelText->setText(text);
}

std::string Label::getLabelText() {
	return _labelText->getText().createUTF8EncodedString();
}

void Label::reDraw(double windowWidth, double windowHeight, bool isRelativePos){
	if(isRelativePos)
		_labelPos = Vec2f(windowWidth*(_labelPos.x()/100), windowHeight*(_labelPos.y()/100));

	
	_labelText->setPosition(osg::Vec3(_labelPos.x(), _labelPos.y(), 0));
}

void Label::initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos) {}
void Label::initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos) {}
void Label::initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos){}