#ifndef CIRCLEHUD_H
#define CIRCLEHUD_H

#include "HudProjection.h"

namespace osgTCC {
	class CircleHUD : public HudProjection {
	public:
		CircleHUD(double windowWidth, double windowHeight, float radius, osg::Vec2f centerPos,
			float alpha, int renderPriority, bool isRelativePos, osg::Vec4 HUDcolor = osg::Vec4(0,0,1,1));
		void reDraw(double windowWidth, double windowHeight, float radius,
					osg::Vec2f centerPos, bool isRelativePos);
		float getRadius() { return _radius; }

	protected:
		virtual void initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos);
		virtual void initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos);
		virtual void initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos);
		float _radius;
	};
}
#endif /* CIRCLEHUD_H */