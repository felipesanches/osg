#include "CircleHUD.h"
#include <math.h>

using namespace osgTCC;
using namespace osg;

CircleHUD::CircleHUD(double windowWidth, double windowHeight, float radius,
					 osg::Vec2f centerPos, float alpha, int renderPriority, bool isRelativePos, osg::Vec4 HUDcolor) 
	: HudProjection(windowWidth, windowHeight, alpha, renderPriority) {
	_HUDcolor->push_back(HUDcolor);
	initCircleBackground(radius, centerPos, isRelativePos);
}

void CircleHUD::initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos) {
	float x;
	float y;

	_HUDBackgroundVertices = new Vec3Array();

	if(!isRelativePos){
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x(), centerPos.y(), -1));
		for(int i = 0; i < 360; i = i + 20) {
			x = centerPos.x() + radius*cosf(DegreesToRadians((float)i));
			y = centerPos.y() + radius*sinf(DegreesToRadians((float)i));
			_HUDBackgroundVertices->push_back(Vec3(x, y, -1));
		}
	}
	else{
		float posX = _windowWidth*(centerPos.x()/100);
		float posY = _windowHeight*(centerPos.y()/100);

		_HUDBackgroundVertices->push_back(Vec3(posX, posY, -1));
		for(int i = 0; i < 360; i = i + 20) {
			x = posX + radius*cosf(DegreesToRadians((float)i));
			y = posY + radius*sinf(DegreesToRadians((float)i));
			_HUDBackgroundVertices->push_back(Vec3(x, y, -1));
		}
	}

	_HUDBackgroundIndices = new DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
	_HUDBackgroundIndices->push_back(0);
	_HUDBackgroundIndices->push_back(1);
	_HUDBackgroundIndices->push_back(18);
	for(int j = 18; j >= 2; j--) {
		_HUDBackgroundIndices->push_back(0);
		_HUDBackgroundIndices->push_back(j);
		_HUDBackgroundIndices->push_back(j-1);
	}

	_HUDBackground->setVertexArray(_HUDBackgroundVertices);
	_HUDBackground->addPrimitiveSet(_HUDBackgroundIndices);
	_HUDBackground->setColorArray(_HUDcolor);
	_HUDBackground->setColorBinding(Geometry::BIND_OVERALL);

	_radius = radius;
}

void CircleHUD::reDraw(double windowWidth, double windowHeight, float radius, osg::Vec2f centerPos, bool isRelativePos){
	//Primeiramente removemos o _HUDbackground dos drawbles do geode da HUD
	_HUDGeode->removeDrawable(_HUDBackground);

	//Criamos um novo geometry
	_HUDBackground = new osg::Geometry();
	//Inicializamos o geometry criado
	initCircleBackground(radius, centerPos, isRelativePos);
	
	//Adicionamos novamente o _HUDbackground aos drawbles do geode da HUD
	_HUDGeode->addDrawable(_HUDBackground);

	//Informamos o resize para a matrix responsável pela ortogonalidade 
	setMatrix(osg::Matrix::ortho2D(0, windowWidth, 0, windowHeight));

	this->_windowWidth = windowWidth;
	this->_windowHeight = windowHeight;
}

void CircleHUD::initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos) {}
void CircleHUD::initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos) {}