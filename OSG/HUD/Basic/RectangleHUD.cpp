#include "RectangleHUD.h"

using namespace osgTCC;
using namespace osg;

RectangleHUD::RectangleHUD(double windowWidth, double windowHeight, float HUDwidth, float HUDheight,
						   osg::Vec2f centerPos, float alpha, int renderPriority, bool isRelativePos, osg::Vec4 HUDcolor)
	: HudProjection(windowWidth, windowHeight, alpha, renderPriority) {
	_HUDcolor->push_back(HUDcolor);
	initRectangleBackground(HUDwidth, HUDheight, centerPos, isRelativePos);
}

void RectangleHUD::initRectangleBackground(float HUDwidth, float HUDheight, osg::Vec2f centerPos, bool isRelativePos) {
	float halfWidth = HUDwidth/2;
	float halfHeight = HUDheight/2;

	_HUDBackgroundVertices = new Vec3Array();
	if(!isRelativePos){
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() - halfWidth, centerPos.y() - halfHeight, -1));
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() - halfWidth, centerPos.y() + halfHeight, -1));
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() + halfWidth, centerPos.y() + halfHeight, -1));
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() + halfWidth, centerPos.y() - halfHeight, -1));
	}
	else{
		float posX = _windowWidth*(centerPos.x()/100);
		float posY = _windowHeight*(centerPos.y()/100);

		_HUDBackgroundVertices->push_back(Vec3(posX - halfWidth, posY - halfHeight, -1));
		_HUDBackgroundVertices->push_back(Vec3(posX - halfWidth, posY + halfHeight, -1));
		_HUDBackgroundVertices->push_back(Vec3(posX + halfWidth, posY + halfHeight, -1));
		_HUDBackgroundVertices->push_back(Vec3(posX + halfWidth, posY - halfHeight, -1));
	}
	
	_HUDBackgroundIndices = new DrawElementsUInt(osg::PrimitiveSet::POLYGON, 0);
	_HUDBackgroundIndices->push_back(0);
	_HUDBackgroundIndices->push_back(1);
	_HUDBackgroundIndices->push_back(2);
	_HUDBackgroundIndices->push_back(3);

	_HUDBackground->setVertexArray(_HUDBackgroundVertices);
	_HUDBackground->addPrimitiveSet(_HUDBackgroundIndices);
	_HUDBackground->setColorArray(_HUDcolor);
	_HUDBackground->setColorBinding(Geometry::BIND_OVERALL);

	_HUDwidth = HUDwidth;
	_HUDheight = HUDheight;
}

void RectangleHUD::reDraw(double windowWidth, double windowHeight, float HUDwidth,
						  float HUDheight, osg::Vec2f centerPos, bool isRelativePos){
	//Primeiramente removemos o _HUDbackground dos drawbles do geode da HUD
	_HUDGeode->removeDrawable(_HUDBackground);

	this->_windowWidth = windowWidth;
	this->_windowHeight = windowHeight;

	//Criamos um novo geometry
	_HUDBackground = new osg::Geometry();
	//Inicializamos o geometry criado
	initRectangleBackground(HUDwidth, HUDheight, centerPos, isRelativePos);
	
	//Adicionamos novamente o _HUDbackground aos drawbles do geode da HUD
	_HUDGeode->addDrawable(_HUDBackground);

	//Informamos o resize para a matrix responsável pela ortogonalidade 
	setMatrix(osg::Matrix::ortho2D(0, windowWidth, 0, windowHeight));
}

void RectangleHUD::initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos) {}
void RectangleHUD::initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos) {}