#ifndef HUDPROJECTION_H
#define HUDPROJECTION_H

#include <osg/Projection>
#include <osg/MatrixTransform>
#include <osg/Geode>
#include <osg/Drawable>
#include <osg/Geometry>
#include <osg/Texture2D>
#include <osgDB/ReadFile>
#include <osg/PositionAttitudeTransform>
#include <osg/Material>
#include <osg/BlendFunc>
#include <osgText/Text>
#include <osgText/Font>

#include <string>

namespace osgTCC {
	class HudProjection : public osg::Projection {
	
	public:
		HudProjection(double windowWidth, double windowHeight, float alpha, int renderPriority);
		osg::Geode* getGeode();
		osg::Geode* getSecondaryGeode();
		void setPositionTransf(osg::Vec3f position);
		void setScaleTransf(osg::Vec3d scale);
		bool setTexture(char* fileName, osg::Vec2Array* texcooords);
		virtual void resize(double windowWidth, double windowHeight);

	protected:
		virtual ~HudProjection() {}
		osg::ref_ptr<osg::MatrixTransform> _HUDModelViewMatrix;
		osg::ref_ptr<osg::Geode> _HUDGeode;
		osg::ref_ptr<osg::Geode> _SecondaryGeode;
		osg::ref_ptr<osg::Geometry> _HUDBackground;
		osg::ref_ptr<osg::Vec3Array> _HUDBackgroundVertices;
		osg::ref_ptr<osg::DrawElementsUInt> _HUDBackgroundIndices;
		osg::ref_ptr<osg::Vec4Array> _HUDcolor;
		osg::ref_ptr<osg::PositionAttitudeTransform> _HUDtransform;
		osg::ref_ptr<osg::StateSet> _HUDStateSet;
		osg::ref_ptr<osg::Material> _HUDMaterial;
		osg::ref_ptr<osg::BlendFunc> _blendFunc;
		osg::ref_ptr<osg::Texture2D> _HUDTexture;
		osg::ref_ptr<osg::Vec3Array> _HUDnormals;
		osg::ref_ptr<osgText::Text> _HUDtext;
		float _windowWidth;
		float _windowHeight;

		virtual void initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos) = 0;
		virtual void initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos) = 0;
		virtual void initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos) = 0;
	};
}
#endif /* HUDPROJECTION_H */
