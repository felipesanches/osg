#include "HudProjection.h"

using namespace osg;
using namespace osgTCC;

HudProjection::HudProjection(double windowWidth, double windowHeight, float alpha, int renderPriority) 
	: Projection() {

	_windowWidth = windowWidth;
	_windowHeight = windowHeight;

	setMatrix(Matrix::ortho2D(0, windowWidth, 0, windowHeight));
	
	_HUDcolor = new Vec4Array();

	//Criamos a matrix do view para essa projecao
	_HUDModelViewMatrix = new MatrixTransform();
	_HUDModelViewMatrix->setMatrix(Matrix::identity());

	//Como a HUD � uma perspectiva com angulo de 90 graus devemos garantir que essa matrix do view nao sofra
	//Nenhuma transformada
	_HUDModelViewMatrix->setReferenceFrame(Transform::ABSOLUTE_RF);

	//Adicionamos o matrix do view como child da projection
	this->addChild(_HUDModelViewMatrix);

	//Instanciamos o n� que representa as transformadas sobre a HUD
	_HUDtransform = new PositionAttitudeTransform();
	_HUDtransform->setPosition(osg::Vec3f(0,0,0));
	_HUDModelViewMatrix->addChild(_HUDtransform);

	//Instanciamos um geode para o HUD. Ser� no geode que ser�o adicionados todos os elementos a serem desenhados na HUD
	_HUDGeode = new Geode();
	_HUDtransform->addChild(_HUDGeode);

	//Criamos um geode extra independente que n�o possuir� o mesmo state set do geode acima (geode para a HUD)
	_SecondaryGeode = new Geode();
	_HUDtransform->addChild(_SecondaryGeode);

	//Criamos um geometry para o HUD. 
	_HUDBackground = new Geometry();
	_HUDGeode->addDrawable(_HUDBackground);
	_HUDGeode->setDataVariance(osg::Object::DYNAMIC);

	//Por fim criamos um stateset para o HUD para garantir algumas propriedades a esse
	_HUDStateSet = new osg::StateSet(); 
	_HUDGeode->setStateSet(_HUDStateSet);

	//Criamos um material. Esse material ser� atribuido ao stateset e tera a propriedade de transparencia desejada
	_HUDMaterial = new Material();
	_HUDStateSet->setMode(GL_BLEND, StateAttribute::ON | StateAttribute::OVERRIDE);
	//Habilitamos a transparencia e definimos o nivel desta atraves da variavel alpha
	_HUDMaterial->setTransparency(Material::FRONT_AND_BACK, alpha);
	//Habilitamos a coloracao do material
	_HUDMaterial->setColorMode(Material::ColorMode::AMBIENT);
	_HUDStateSet->setAttributeAndModes(_HUDMaterial, StateAttribute::ON | StateAttribute::OVERRIDE);

	//Desabilita o depth test assim o geometry ser� desenhado independendo do valor de depth
	_HUDStateSet->setMode(GL_DEPTH_TEST,osg::StateAttribute::OFF);
	//_HUDStateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

	//Temos que fazer esse geometry ser o ultimo a ser desenhado. RenderBins 
	//s�o manejados de ordem numerica assim se colocarmos um numero grande
	//garantimos que o HUD ser� o ultimo
	//OBS VER COMO SETAR ESSE NUM AUTOMATICAMENTE
	_HUDStateSet->setRenderBinDetails(renderPriority, "RenderBin");

}

Geode* HudProjection::getGeode() {
	return _HUDGeode;
}

Geode* HudProjection::getSecondaryGeode(){
	return _SecondaryGeode;
}

void HudProjection::setPositionTransf(osg::Vec3f position) {
	_HUDtransform->setPosition(position);
}

void HudProjection::setScaleTransf(osg::Vec3d scale) {
	_HUDtransform->setScale(scale);
}


bool HudProjection::setTexture(char* fileName, Vec2Array* texcoords) {

	if(texcoords == NULL) {
		Vec2Array* texcoords = new osg::Vec2Array(4);
		(*texcoords)[0].set(0.0f,0.0f);
		(*texcoords)[1].set(1.0f,0.0f);
		(*texcoords)[2].set(1.0f,1.0f);
		(*texcoords)[3].set(0.0f,1.0f);
	}
	//Setamos as coordenadas
	_HUDBackground->setTexCoordArray(0,texcoords);

	//Criamos o tipo de textura
	_HUDTexture = new Texture2D();
	//Setamos a variance
	_HUDTexture->setDataVariance(Object::DYNAMIC); 

	//Lemos a imagem
	ref_ptr<Image> img = osgDB::readImageFile(fileName);
	if(img == NULL)
		return false;
	else
		_HUDTexture->setImage(0, img);

	//Criamos o vetor que ser� a normal da HUD
	_HUDnormals = new Vec3Array;
	_HUDnormals->push_back(osg::Vec3(0.0f,-1.0f,0.0f));
	_HUDBackground->setNormalArray(_HUDnormals);
	_HUDBackground->setNormalBinding(Geometry::BIND_OVERALL);
	
	//Informos ao HUDstate sobre a textura
	_HUDStateSet->setTextureAttributeAndModes(0,_HUDTexture,osg::StateAttribute::ON);

	return true;
}

void HudProjection::resize(double windowWidth, double windowHeight){
	setMatrix(osg::Matrix::ortho2D(0, windowWidth, 0, windowHeight));
	_windowWidth = windowWidth;
	_windowHeight = windowHeight;
}