#include "StatsHUD.h"
#include "../../Callbacks/HUD/StatsHUDCallback.h"

using namespace osgTCC;
using namespace osg;
using namespace osgViewer;

StatsHUD::StatsHUD(double windowWidth, double windowHeight, EventStatus *eventStatus) 
	: RectangleHUD(windowWidth, windowHeight, 80, 40, Vec2(50, 20), 1, INT_MAX - 2, false, osg::Vec4(0, 0, 0, 0)) {
	// Criamos o label utilizado
	_fpsLabel = new Label(windowWidth, windowHeight, Vec2(10, 20), 0, INT_MAX - 5, false);
	_fpsLabel->setLabelText(20, "", osg::Vec4f(1, 1, 1, 1));
	this->addChild(_fpsLabel);

	// Desabilitamos a HUD
	_isShowingStats = false;

	// Inicializamos o timer
	_timer.setStartTick();

	// Associamos o callback utilizado
	addUpdateCallback(new StatsHUDCallback(eventStatus));
}

void StatsHUD::toggle() {
	// Apenas trocamos o status da flag
	if (_isShowingStats)
		_isShowingStats = false;
	else
		_isShowingStats = true;
}

void StatsHUD::updateFPS() {
	// Devemos obter o valor corrente do timer
	double fps = (double) 1000 / _timer.time_m();
	std::stringstream ss;
	ss.precision(2);
	ss << std::fixed << fps << " fps";
	_fpsLabel->setLabelText(ss.str()); 
	// Resetamos o timer
	_timer.setStartTick();
}

bool StatsHUD::isShowing() {
	return _isShowingStats;
}