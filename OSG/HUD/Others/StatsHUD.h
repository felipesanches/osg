#ifndef __STATSHUD_H__
#define __STATSHUD_H__

#include <osgGA/GUIEventHandler>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>
#include <osgViewer/Viewer>
#include <osg/Timer>

#include "../../HUD/Basic/RectangleHUD.h"
#include "../../HUD/Basic/Label.h"

namespace osgTCC {
	class EventStatus;

	class StatsHUD : public RectangleHUD {
	public:
		StatsHUD(double windowWidth, double windowHeight, EventStatus *eventStatus);
		
		// Torna vis�vel/oculta a HUD
		void toggle();

		// Atualiza o valor corrente do FPS
		void updateFPS();

		// Informa se a HUD deve ser mostrada
		bool isShowing();
		
	protected:
		// Destrutor protegido
		virtual ~StatsHUD() {}

		// Armazena o label utilizado para mostrar o FPS
		osg::ref_ptr<Label> _fpsLabel;

		// Informa se a HUD est� sendo mostrada
		bool _isShowingStats;

		// Armazena o timer utilizado para calcular o fps
		osg::Timer _timer;

	};

}
#endif // !__STATSHUD_H__
