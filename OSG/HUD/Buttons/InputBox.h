#ifndef INPUTBOX_H
#define INPUTBOX_H

#include "Button.h"

namespace osgTCC {
	class InputBox : public Button {
	public:
		InputBox(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight,
					osg::Vec2f pos, float alpha, bool isRelativePos, osg::Vec4 buttonColor);
		InputBox(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight,
					osg::Vec2f pos, float alpha, int renderPriority, bool isRelativePos, osg::Vec4 buttonColor);
		void updateWritingFlag();
		void writeLetter(char letter);
		bool InputBox::getWritingFlag();
		void InputBox::setWritingFlag(bool isWriting);

	protected:
		// Destrutor protegido.
		virtual ~InputBox() {}
		void removeWritingFlag();

		int _countToRevert;
		bool _isWritingPath;
		bool _writeFlag;
	};
}
#endif /* INPUTBOX_H */