#include "Button.h"

#include <iostream>

using namespace osg;
using namespace osgTCC;

Button::Button(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight, Vec2f pos,
							float alpha, bool isRelativePos, osg::Vec4 buttonColor) {
	_buttonHUD = new RectangleHUD(windowWidth, windowHeight, buttonWidth, buttonHeight, pos, alpha, INT_MAX,
						isRelativePos, buttonColor);

	initialize(windowWidth, windowHeight, buttonWidth, buttonHeight, pos, alpha, isRelativePos);
}

Button::Button(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight, Vec2f pos,
							float alpha, int renderPriority, bool isRelativePos, osg::Vec4 buttonColor){
	_buttonHUD = new RectangleHUD(windowWidth, windowHeight, buttonWidth, buttonHeight, pos,
									alpha, renderPriority, isRelativePos, buttonColor);

	initialize(windowWidth, windowHeight, buttonWidth, buttonHeight, pos, alpha, isRelativePos);

	confPriorityStateSet(renderPriority);
}

void Button::initialize(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight, Vec2f pos,
											float alpha, bool isRelativePos){

	_buttonText = new osgText::Text();
	_buttonText->setDataVariance(osg::Object::DYNAMIC);
	_buttonHUD->getSecondaryGeode()->addDrawable(_buttonText);

	if(!isRelativePos)
		_buttonPos = Vec2f(pos.x(), pos.y());
	else
		_buttonPos = Vec2f(windowWidth*(pos.x()/100), windowHeight*(pos.y()/100));

	_buttonWidth = buttonWidth;
	_buttonHeight = buttonHeight;
	_windowWidth = windowWidth;
	_windowHeight = windowHeight;

	confAlphaStateSet(alpha);
}

void Button::confAlphaStateSet(float alpha){
	if(alpha == 1){
		_buttonHUD->getGeode()->getOrCreateStateSet()->setMode(GL_BLEND, StateAttribute::OFF | StateAttribute::OVERRIDE);
		_buttonHUD->getSecondaryGeode()->getOrCreateStateSet()->setMode(GL_BLEND, StateAttribute::OFF | StateAttribute::OVERRIDE);
	}
	else{
		_buttonHUD->getGeode()->getOrCreateStateSet()->setMode(GL_BLEND, StateAttribute::ON | StateAttribute::OVERRIDE);
		_buttonHUD->getSecondaryGeode()->getOrCreateStateSet()->setMode(GL_BLEND, StateAttribute::ON | StateAttribute::OVERRIDE);
	}
	
}

void Button::confPriorityStateSet(int renderPriority){
	_buttonHUD->getGeode()->getOrCreateStateSet()->setRenderBinDetails(renderPriority + 1, "RenderBin");
	_buttonHUD->getSecondaryGeode()->getOrCreateStateSet()->setRenderBinDetails(renderPriority + 2, "RenderBin");
}

RectangleHUD* Button::getButtonHud() {
	return _buttonHUD;
}

void Button::setButtonText(int fontSize, std::string buttonText, buttonTextPos textPos, osg::Vec4f textColor) {
	// Set up the parameters for the text we'll add to the HUD:
    _buttonText->setCharacterSize(fontSize);
    _buttonText->setFont("C:/WINDOWS/Fonts/impact.ttf");
    _buttonText->setText(buttonText);
    //_buttonText->setAxisAlignment(osgText::Text::SCREEN);

	_textPos = textPos;

	switch (textPos)
	{
	case osgTCC::LEFT:
		_buttonText->setPosition(Vec3f(_buttonPos.x() - _buttonWidth/2, _buttonPos.y() - _buttonHeight/4, 0));
		break;
	case osgTCC::CENTER:
		_buttonText->setPosition(Vec3f(_buttonPos.x() - _buttonHeight/2, _buttonPos.y() - _buttonHeight/3, 0));
		break;
	case osgTCC::RIGHT:
		_buttonText->setPosition(Vec3f(_buttonPos.x() + _buttonWidth/2, _buttonPos.y() - _buttonHeight/4, 0));
		break;
	}
    _buttonText->setColor(textColor);
}

void Button::setButtonText(std::string text) {
	_buttonText->setText(text);
}

std::string Button::getButtonText() {
	return _buttonText->getText().createUTF8EncodedString();
}

bool Button::getCollision(float x, float y) {
	if(x >= (_buttonPos.x() - _buttonWidth/2 ) && x <= (_buttonPos.x() + _buttonWidth/2)) {
		if(y >= (_buttonPos.y() - _buttonHeight/2) && y <= (_buttonPos.y() + _buttonHeight/2)) {
			return true;
		}
	}
	return false;
}

void Button::reDraw(double windowWidth, double windowHeight, float HUDwidth, float HUDheight,
						osg::Vec2f centerPos, bool isRelativePos){
	_buttonHUD->reDraw(windowWidth, windowHeight, HUDwidth, HUDheight, centerPos, isRelativePos);

	if(!isRelativePos)
		_buttonPos = Vec2f(centerPos.x(), centerPos.y());
	else
		_buttonPos = Vec2f(windowWidth*(centerPos.x()/100), windowHeight*(centerPos.y()/100));


	//por fim reposicionamos o texto do botao
	switch (_textPos)
	{
	case osgTCC::LEFT:
		_buttonText->setPosition(Vec3f(_buttonPos.x() - HUDwidth/2, _buttonPos.y() - HUDheight/4, 0));
		break;
	case osgTCC::CENTER:
		_buttonText->setPosition(Vec3f(_buttonPos.x() - HUDheight/2, _buttonPos.y() - HUDheight/3, 0));
		break;
	case osgTCC::RIGHT:
		_buttonText->setPosition(Vec3f(_buttonPos.x() + HUDwidth/2, _buttonPos.y() - HUDheight/4, 0));
		break;
	}
}