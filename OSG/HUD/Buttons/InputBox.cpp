#include "InputBox.h"

using namespace osgTCC;

InputBox::InputBox(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight,
					osg::Vec2f pos, float alpha, bool isRelativePos, osg::Vec4 buttonColor) : 
Button(windowWidth, windowHeight, buttonWidth, buttonHeight, pos, alpha, isRelativePos, buttonColor){
	// Quando atinge 20 inverte a situacao do cursor de escrita (faz ele piscar na tela)
	_countToRevert = 21;
	_isWritingPath = false;
	_writeFlag = false;
}

InputBox::InputBox(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight,
					osg::Vec2f pos, float alpha, int renderPriority, bool isRelativePos, osg::Vec4 buttonColor) :
Button(windowWidth, windowHeight, buttonWidth, buttonHeight, pos, alpha, renderPriority, isRelativePos, buttonColor){
	// Quando atinge 20 inverte a situacao do cursor de escrita (faz ele piscar na tela)
	_countToRevert = 21;
	_isWritingPath = false;
	_writeFlag = false;
}

void InputBox::updateWritingFlag(){
	if(_isWritingPath == false){
		removeWritingFlag();
	}
	else{
		_countToRevert = _countToRevert + 1;
		if(_countToRevert > 20){
			if(_writeFlag){
				removeWritingFlag();
				_writeFlag = false;
			}
			else{
				_buttonText->setText(getButtonText() + "|");
				_writeFlag = true;
			}
			_countToRevert = 0;
		}
	}
}

void InputBox::writeLetter(char letter){
	// Remove o ponteiro | da string
	removeWritingFlag();

	std::string text = getButtonText();
	//Verifica se � o backspace
	if(letter == 8 && text.length() > 0){
		text.resize(text.length() - 1);
	}
	else if(letter != 8){
		text = text + letter;
	}
	
	//Coloca a flag | no final caso ela esteja ativa
	if(_writeFlag){
		text = text + "|";
	}
	_buttonText->setText(text);
}

void InputBox::removeWritingFlag(){
	if(_writeFlag){
		std::string text = getButtonText();
		text.resize(text.length() - 1);
		_buttonText->setText(text);
		_writeFlag = false;
	}
}

bool InputBox::getWritingFlag(){
	return _isWritingPath;
}

void InputBox::setWritingFlag(bool isWriting){
	if(isWriting){
		_countToRevert = 21;
		_isWritingPath = isWriting;
	}
	else{
		_isWritingPath = isWriting;
	}
}