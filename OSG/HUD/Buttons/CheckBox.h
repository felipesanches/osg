#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "Button.h"

namespace osgTCC {
	class CheckBox : public Button {
	public:
		CheckBox(float windowWidth, float windowHeight, float edgeLength, osg::Vec2f pos, float alpha,
			bool isRelativePos, std::string checkLabel, osg::Vec4 labelColor, osg::Vec4 buttonColor);
		CheckBox(float windowWidth, float windowHeight, float edgeLength, osg::Vec2f pos, float alpha,
			int renderPriority, bool isRelativePos, std::string checkLabel, osg::Vec4 labelColor, osg::Vec4 buttonColor);
		void setButtonText();
		bool getCheckBoxMark();
		void clearMark();
		void reDraw(double windowWidth, double windowHeight, float HUDwidth, float HUDheight,
						osg::Vec2f centerPos, bool isRelativePos);
	
	protected:
		// Destrutor protegido.
		virtual ~CheckBox() {}
		void initialize(std::string label, osg::Vec4 labelColor, float edgeLength);

		int _fontSize;
		std::string _labelText;

		//Usado para dar um nome ao checkbox
		osg::ref_ptr<osgText::Text> _label;

		int _delay;
	};
}
#endif /* CHECKBOX_H */