#ifndef FREECAMERAMANIPULATOR_H
#define FREECAMERAMANIPULATOR_H

#include <osgGA/CameraManipulator>
#include <osg/Camera>
#include <osg/Quat>
#include <osg/Matrix>

#include "../UserDatas/EventStatus.h"

namespace osgTCC {
	class FreeCameraManipulator : public osgGA::CameraManipulator {
	public:
		FreeCameraManipulator(osg::Vec3f initPos, EventStatus *eventStatus);

		// Metodo usada para informada a camera das mudan�as na matrix view. Esse metodo � chamado pelo Viewer
		virtual void updateCamera(osg::Camera &camera);

		// Metodos para setar a posicao da camera atraves de matrix
		virtual void setByMatrix(const osg::Matrixd &matrix);
		virtual void setByInverseMatrix(const osg::Matrixd &matrix);

		virtual osg::Matrixd getMatrix() const;
		virtual osg::Matrixd getInverseMatrix() const;

	protected:
		//Destruidor da class
		~FreeCameraManipulator() {}

		// Vetores que compoem o eye, center. Respectivamente sao _target e _Href
		osg::Vec3f _target;
		osg::Vec3f _Href;

		// Matrix de transformada da camera
		osg::Matrixd _cameraRotation;
		osg::Matrixd _cameraTranslation;

		// Matrix com transformadas concatenadas
		osg::Matrixd _cameraMatrix;

		// Armazena a angulacao da camera
		float _rollAngle;
		float _headingAngle;

		// Indica se a camera esta travada ou nao
		bool _isDisabled;

		// Armazena o variacao do espaco que o mouse andou
		float _mouseDeltaX;
		float _mouseDeltaY;

		// Armazena a ultima posicao do mouse
		int _lastMouseX;
		int _lastMouseY;

		// Armazena as dimensoes da janela
		int _windowWidth;
		int _windowHeight;

		// Posicao da camera
		osg::Vec3f _position;
		// Usado para fazer uma copida da posicao da camera
		osg::Vec3f _positionBackup;

		EventStatus* _eventStatus;

		// Constantes da camera
		float _cornerLimitX;
		float _cornerLimitY;
		float _velocity;
		float _angleStep;

		// Metodos que verificam se o mouse esta em algum dos cantos da tela
		bool isAtHorizontalCorner();
		bool isAtVerticalCorner();

		// Metodo responsavel por transformar movimento do mouse em rotacao
		void mouseMoviment();

		// Realiza o movimento da camera dependendo da situ��o
		void freeMove();
		void editorMove();
	};
}
#endif /* FREECAMERAMANIPULATOR_H */