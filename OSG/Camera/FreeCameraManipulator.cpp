#include "FreeCameraManipulator.h"

#include <iostream>

using namespace osgTCC;

void normalizeAngle(float &angle);

FreeCameraManipulator::FreeCameraManipulator(osg::Vec3f initPos, EventStatus *eventStatus) : CameraManipulator(){
	// Transladamos a camera para a posicao inicial
	_cameraTranslation.makeTranslate(initPos);
	// Rotacionamos a camera para a posicao inicial
	_cameraRotation.makeRotate(osg::DegreesToRadians(0.0), osg::Vec3d(1,0,0),
							   osg::DegreesToRadians(0.0), osg::Vec3d(0,1,0),
							   osg::DegreesToRadians(0.0), osg::Vec3d(0,0,1));

	// Ligamos a camera
	_isDisabled = false;


	_eventStatus = eventStatus;
	_rollAngle = -150;
	_headingAngle = 0;
	_position = initPos;
	_positionBackup = initPos;
	
	//Inicia constantes
	_angleStep = 0.95f;
	_velocity = 8.0f;

	//Inicia a posicao do mouse
	_mouseDeltaX = 0;
	_mouseDeltaY = 0;
	//O mouse � iniciado no centro da tela
	_lastMouseX = floor(_eventStatus->windowWidth/2);
	_lastMouseY = floor(_eventStatus->windowHeight/2);
}

void FreeCameraManipulator::setByMatrix(const osg::Matrixd &matrix){
	_cameraTranslation.makeTranslate(matrix.getTrans());
}

void FreeCameraManipulator::setByInverseMatrix(const osg::Matrixd &matrix){
	osg::Matrixd invMatrix;
	matrix.inverse(invMatrix);
	_cameraMatrix.makeTranslate(invMatrix.getTrans());
}

osg::Matrixd FreeCameraManipulator::getMatrix() const{
	return _cameraMatrix;
}

osg::Matrixd FreeCameraManipulator::getInverseMatrix() const{
 		osg::Matrixd i = _cameraMatrix.inverse(_cameraMatrix);
		osg::Matrixd iMatrix = osg::Matrix(i.ptr()) * osg::Matrix::rotate(-3.14/2.0, 1, 0, 0);
		return iMatrix;
}

void FreeCameraManipulator::updateCamera(osg::Camera &camera){
	float tempRollAngle;
	float tempHeadingAngle;
	
	if(_eventStatus->resizeEvent){
		_lastMouseX = floor(_eventStatus->windowWidth/2);
		_lastMouseY = floor(_eventStatus->windowHeight/2);
		_eventStatus->resizeEvent = false;
	}

	if(_eventStatus->cameraEnabled){
	
		//Tratamos o evento de abertura da tela de edicao
		if(_eventStatus->editorHudStatus == HUD_OPEN){
			_eventStatus->editorHudStatus = HUD_INUSE;
			// Guardamos a posicao da camera antes de entrar no modo editor
			_positionBackup = _position;
			_position =	osg::Vec3(0, 0, 200);
			tempRollAngle = 270;
			tempHeadingAngle = 0;

		}
		//Tratamos a camera quando a tela de edicao esta fechada
		else if(_eventStatus->editorHudStatus == HUD_NOP){
			if(_eventStatus->freeMouseToggle == MFM_OFF)
				mouseMoviment();
			freeMove();
			tempRollAngle = _rollAngle;
			tempHeadingAngle = _headingAngle;
		}
		//Tratamos a camera quando a tela de edicao esta aberta
		else if(_eventStatus->editorHudStatus == HUD_INUSE) {
			editorMove();
			tempRollAngle = 270;
			tempHeadingAngle = 0;

		}
		//Tratamos o evento de fechamento da tela de edicao
		else if(_eventStatus->editorHudStatus == HUD_CLOSE){
			_eventStatus->editorHudStatus = HUD_NOP;
			// Recuperamos a posicao da camera antes do modo editor
			_position = _positionBackup;
			tempRollAngle = _rollAngle;
			tempHeadingAngle = _headingAngle;
		}

		//Criamos um quarterion para gerar a rota��o entorno do eixo Z
		osg::Quat quarternion = osg::Quat(osg::DegreesToRadians(tempHeadingAngle),	osg::Vec3d(0,0,1));
		//Iniciamos a direcao sempre apontando para o Y
		_target = osg::Vec3d(0,1,0);
		//Realizamos a rotacao do vetor direction em heading graus entorno do eixo Z
		_target = quarternion * _target;

		//Iniciamos o vetor Up apontando para cima (Z+)
		osg::Vec3d up = osg::Vec3d(0,0,1);
		//Criamos a referencia vertical atravez do produto vetorial de Up com o direction
		//Ja rotacionado
		_Href = up^_target;

		//Atualizamos o quarternion para a nova rotacao que agora ser� entorno do eixo de 
		//Referencia criado em roll graus
		quarternion = osg::Quat(osg::DegreesToRadians(tempRollAngle), _Href);
		_target = quarternion * _target;

		//Nao sei pqmas o z esta saindo positivo pra baixo entao tenho q mudar o sinal
		_target.z() = -_target.z();

		_cameraRotation.makeRotate(osg::DegreesToRadians(tempRollAngle), osg::Vec3f(1,0,0),
							   osg::DegreesToRadians(0.0), osg::Vec3f(0,1,0),
							   osg::DegreesToRadians(tempHeadingAngle), osg::Vec3f(0,0,1));
		_cameraTranslation.makeTranslate(_position);


		//Calcula a matrix final da camera e atualiza o manipulador da camera
		_cameraMatrix = _cameraRotation * _cameraTranslation;

		camera.setViewMatrix(getInverseMatrix());
	}
}

void FreeCameraManipulator::mouseMoviment(){
	//Atualiza o delta do mouse
	_mouseDeltaX = (_eventStatus->mouseX - _lastMouseX)/6.0f;
	_mouseDeltaY = (_eventStatus->mouseY - _lastMouseY)/6.0f;

	//Atualiza o angulo da camera
	_rollAngle = _rollAngle - _mouseDeltaY;
	_headingAngle = _headingAngle - _mouseDeltaX;

	normalizeAngle(_rollAngle);
	normalizeAngle(_headingAngle);
}

void normalizeAngle(float &angle){
	bool isNegative = false;

	if(angle < 0){
		isNegative = true;
		angle *= -1;
	}
	angle -= (int)(angle/360) * 360;

	if(isNegative)
		angle = 360 - angle;
}

void FreeCameraManipulator::freeMove(){
	// Decidimos quem
	osg::Vec3d up = _target ^ _Href;
	switch (_eventStatus->camMovimentDirection) {
	case CAMERA_FORWARD:
		_position = _position + _target * _velocity;
		break;
	case CAMERA_BACKWARD:
		_position = _position - _target * _velocity;
		break;
	case CAMERA_LEFT:
		_position = _position + _Href * _velocity;
		break;
	case CAMERA_RIGHT:
		_position = _position - _Href * _velocity;
		break;
	case CAMERA_UP:
		_position = _position + up * _velocity;
		break;
	case CAMERA_DOWN:
		_position = _position - up * _velocity;
		break;
	}
	_eventStatus->camMovimentDirection = CAMERA_NOP;
}

void FreeCameraManipulator::editorMove(){
	// Decidimos quem
	osg::Vec3d up = _target ^ _Href;
	switch (_eventStatus->camMovimentDirection) {
	case CAMERA_FORWARD:
		_position = _position + up * _velocity;
		break;
	case CAMERA_BACKWARD:
		_position = _position - up * _velocity;
		break;
	case CAMERA_LEFT:
		_position = _position + _Href * _velocity;
		break;
	case CAMERA_RIGHT:
		_position = _position - _Href * _velocity;
		break;
	case CAMERA_UP:
		_position.z() = _position.z() + _velocity;
		break;
	case CAMERA_DOWN:
		_position.z() = _position.z() - _velocity;
		break;
	}
	_eventStatus->camMovimentDirection = CAMERA_NOP;
}