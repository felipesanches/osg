#ifndef __WINDOWEDVIEWER_H__
#define __WINDOWEDVIEWER_H__

#include <osgViewer/Viewer>

namespace osgTCC {
	class WindowedViewer : public osgViewer::Viewer {
	public:
		// Construtor que cria um viewer em uma janela.
		WindowedViewer(const std::string &windowName, int width, int height,
			int xPosition, int yPosition);
		// Define o nome da janela.
		void setWindowName(const std::string &name);
	};
}
#endif // !__WINDOWEDVIEWER_H__