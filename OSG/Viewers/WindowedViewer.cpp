#include "WindowedViewer.h"

#include "../Utils/Utils.h"

#include <osgViewer/config/SingleWindow>

using namespace osgViewer;
using namespace osgTCC;

WindowedViewer::WindowedViewer(const std::string &windowName, int width,
							   int height, int xPosition, int yPosition)
	: Viewer() {
	// Devemos inicializar um novo SingleWindow
	osg::ref_ptr<SingleWindow> singleWindow =
	 new SingleWindow(xPosition, yPosition, width, height);
	
	// Definimos a cor de fundo (azul celeste)
	getCamera()->setClearColor(osg::Vec4(0.2, 0.6, 0.8, 0));

	// Aplicamos o tipo da janela
	apply(singleWindow);
	
	// Definimos o nome da janela
	setWindowName(windowName);
}

void WindowedViewer::setWindowName(const std::string &name) {
	// Mudamos o nome da janela
	osg::ref_ptr<osg::GraphicsContext::Traits> traits =
		new osg::GraphicsContext::Traits(
		*getCamera()->getGraphicsContext()->getTraits());
	traits->windowName = name;

	//Antialiasing
	traits->samples = 8;
	
	osg::ref_ptr<osg::GraphicsContext> gc =
		osg::GraphicsContext::createGraphicsContext(traits);

	getCamera()->setGraphicsContext(gc);
}
