#include "CubeGeode.h"

#include "../Utils/Utils.h"

#include <osg/ShapeDrawable>
#include <osg/Geometry>

using namespace osg;
using namespace osgTCC;

CubeGeode::CubeGeode(float edgeLength, const Vec3 &position) : Geode() {
	// Checamos se o valor � v�lido
	if (edgeLength <= 0) 
		showErrorMessage("A aresta do cubo informada nao e valida.", 6);
	// Checamos se a posi��o n�o � nula
	if ((&position) == NULL)
		showErrorMessage("A posicao do cubo nao pode ser nula.", 6);
	
	// Armazenamos a metade do lado, para facitar a defini��o dos v�rtices:
	float half = edgeLength / 2;

	// Criamos a geometria que conter� o cubo, e adicionamos ao geode:
	ref_ptr<Geometry> geometry = new Geometry();
	addDrawable(geometry);
	
	// Devemos criar as 6 faces do cubo. Para isso, criamos 24 v�rtices:
	ref_ptr<Vec3Array> vertices = new Vec3Array();
	// Frente:
	vertices->push_back(Vec3(position.x() - half, position.y() - half, position.z() - half));
	vertices->push_back(Vec3(position.x() + half, position.y() - half, position.z() - half));
	vertices->push_back(Vec3(position.x() + half, position.y() - half, position.z() + half));
	vertices->push_back(Vec3(position.x() - half, position.y() - half, position.z() + half));
	// Direita:
	vertices->push_back(Vec3(position.x() + half, position.y() - half, position.z() - half));
	vertices->push_back(Vec3(position.x() + half, position.y() + half, position.z() - half));
	vertices->push_back(Vec3(position.x() + half, position.y() + half, position.z() + half));
	vertices->push_back(Vec3(position.x() + half, position.y() - half, position.z() + half));
	// Traseira:
	vertices->push_back(Vec3(position.x() + half, position.y() + half, position.z() - half));
	vertices->push_back(Vec3(position.x() - half, position.y() + half, position.z() - half));
	vertices->push_back(Vec3(position.x() - half, position.y() + half, position.z() + half));
	vertices->push_back(Vec3(position.x() + half, position.y() + half, position.z() + half));
	// Esquerda:
	vertices->push_back(Vec3(position.x() - half, position.y() + half, position.z() - half));
	vertices->push_back(Vec3(position.x() - half, position.y() - half, position.z() - half));
	vertices->push_back(Vec3(position.x() - half, position.y() - half, position.z() + half));
	vertices->push_back(Vec3(position.x() - half, position.y() + half, position.z() + half));
	// Base:
	vertices->push_back(Vec3(position.x() - half, position.y() + half, position.z() - half));
	vertices->push_back(Vec3(position.x() + half, position.y() + half, position.z() - half));
	vertices->push_back(Vec3(position.x() + half, position.y() - half, position.z() - half));
	vertices->push_back(Vec3(position.x() - half, position.y() - half, position.z() - half));
	// Topo:
	vertices->push_back(Vec3(position.x() - half, position.y() - half, position.z() + half));
	vertices->push_back(Vec3(position.x() + half, position.y() - half, position.z() + half));
	vertices->push_back(Vec3(position.x() + half, position.y() + half, position.z() + half));
	vertices->push_back(Vec3(position.x() - half, position.y() + half, position.z() + half));
	
	// Adicionamos os v�rtices rec�m-criados, e seguindo a conven��o, utilizamos o �nidce 6:
	geometry->setVertexArray(vertices);
	geometry->setVertexAttribArray(6, vertices);
	geometry->setVertexAttribBinding(6, Geometry::BIND_PER_VERTEX);

	// Definimos que utilizaremos quads para desenhar o objeto, e adicionamos � geometria:
	ref_ptr<DrawElementsUInt> primitiveSet = new DrawElementsUInt(PrimitiveSet::QUADS);
	for (unsigned int i = 0; i < 24; i++)
		primitiveSet->push_back(i);
	geometry->addPrimitiveSet(primitiveSet);

	// Devemos criar o vetor de normais:
	ref_ptr<Vec3Array> normals = new Vec3Array();
	// Frente:
	normals->push_back(Vec3(0, -1, 0));
	normals->push_back(Vec3(0, -1, 0));
	normals->push_back(Vec3(0, -1, 0));
	normals->push_back(Vec3(0, -1, 0));
	// Direita:
	normals->push_back(Vec3(1, 0, 0));
	normals->push_back(Vec3(1, 0, 0));
	normals->push_back(Vec3(1, 0, 0));
	normals->push_back(Vec3(1, 0, 0));
	// Traseira:
	normals->push_back(Vec3(0, 1, 0));
	normals->push_back(Vec3(0, 1, 0));
	normals->push_back(Vec3(0, 1, 0));
	normals->push_back(Vec3(0, 1, 0));
	// Esquerda:
	normals->push_back(Vec3(-1, 0, 0));
	normals->push_back(Vec3(-1, 0, 0));
	normals->push_back(Vec3(-1, 0, 0));
	normals->push_back(Vec3(-1, 0, 0));
	// Base:
	normals->push_back(Vec3(0, 0, -1));
	normals->push_back(Vec3(0, 0, -1));
	normals->push_back(Vec3(0, 0, -1));
	normals->push_back(Vec3(0, 0, -1));
	// Topo:
	normals->push_back(Vec3(0, 0, 1));
	normals->push_back(Vec3(0, 0, 1));
	normals->push_back(Vec3(0, 0, 1));
	normals->push_back(Vec3(0, 0, 1));
	
	// Adicionamos as normais rec�m-criadas, e seguindo a conven��o, utilizamos o �nidce 7:
	geometry->setNormalArray(normals);
	geometry->setNormalBinding(Geometry::BIND_PER_VERTEX);
	geometry->setVertexAttribArray(7, normals);
	geometry->setVertexAttribBinding(7, Geometry::BIND_PER_VERTEX);

	// Definimos as coordenadas de texturas:
	ref_ptr<Vec2Array> textureCoords = new Vec2Array();
	// Frente:
	textureCoords->push_back(Vec2(0, 0));
	textureCoords->push_back(Vec2(1, 0));
	textureCoords->push_back(Vec2(1, 1));
	textureCoords->push_back(Vec2(0, 1));
	// Direita:
	textureCoords->push_back(Vec2(0, 0));
	textureCoords->push_back(Vec2(1, 0));
	textureCoords->push_back(Vec2(1, 1));
	textureCoords->push_back(Vec2(0, 1));
	// Traseira:
	textureCoords->push_back(Vec2(0, 0));
	textureCoords->push_back(Vec2(1, 0));
	textureCoords->push_back(Vec2(1, 1));
	textureCoords->push_back(Vec2(0, 1));
	// Esquerda:
	textureCoords->push_back(Vec2(0, 0));
	textureCoords->push_back(Vec2(1, 0));
	textureCoords->push_back(Vec2(1, 1));
	textureCoords->push_back(Vec2(0, 1));
	// Base:
	textureCoords->push_back(Vec2(0, 0));
	textureCoords->push_back(Vec2(1, 0));
	textureCoords->push_back(Vec2(1, 1));
	textureCoords->push_back(Vec2(0, 1));
	// Topo:
	textureCoords->push_back(Vec2(0, 0));
	textureCoords->push_back(Vec2(1, 0));
	textureCoords->push_back(Vec2(1, 1));
	textureCoords->push_back(Vec2(0, 1));

	// Por fim, adicionamos as normais rec�m-criadas, e seguindo a conven��o, utilizamos o �nidce 8:
	geometry->setTexCoordArray(0, textureCoords);
	geometry->setVertexAttribArray(8, textureCoords);
	geometry->setVertexAttribBinding(8, Geometry::BIND_PER_VERTEX);
}