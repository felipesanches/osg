#include "SquareGeode.h"

#include "../Utils/Utils.h"

#include <osg/Geometry>

using namespace osg;
using namespace osgTCC;

SquareGeode::SquareGeode(float edgeLength, const Vec3 &position) : Geode() {
	// Checamos se o valor � v�lido
	if (edgeLength <= 0)
		showErrorMessage("A aresta do quadrado informada nao e valida.", 6);
	// Checamos se a posi��o n�o � nula
	if ((&position) == NULL)
		showErrorMessage("A posicao do quadrado nao pode ser nula.", 6);

	// Criamos a geometria e a adicionamos
	ref_ptr<Geometry> squareGeometry = new Geometry();
	addDrawable(squareGeometry);

	// Definimos que usaremos VBOs
	squareGeometry->setUseVertexBufferObjects(true);

	// Especificamos os v�rtices
	float half = edgeLength / 2.0;
	ref_ptr<osg::Vec3Array> squareVertices = new osg::Vec3Array;
	squareVertices->push_back(
		Vec3(position.x() - half, position.y(), position.z() - half));
	squareVertices->push_back(
		Vec3(position.x() + half, position.y(), position.z() - half));
	squareVertices->push_back(
		Vec3(position.x() + half, position.y(), position.z() + half));
	squareVertices->push_back(
		Vec3(position.x() - half, position.y(), position.z() + half));

	// Adicionamos os v�rtices rec�m-criados, e seguindo a conven��o, utilizamos o �nidce 6:
	squareGeometry->setVertexArray(squareVertices);
	squareGeometry->setVertexAttribArray(6, squareVertices);
	squareGeometry->setVertexAttribBinding(6, Geometry::BIND_PER_VERTEX);

	// Definimos que utilizaremos quads para desenhar o quadradro:
	ref_ptr<DrawElementsUInt> squareFace =
		new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS);
	squareFace->push_back(0);
	squareFace->push_back(1);
	squareFace->push_back(2);
	squareFace->push_back(3);
	squareGeometry->addPrimitiveSet(squareFace);

	// Devemos criar o vetor de normais:
	ref_ptr<Vec3Array> normals = new Vec3Array();
	normals->push_back(Vec3(0, -1, 0));
	normals->push_back(Vec3(0, -1, 0));
	normals->push_back(Vec3(0, -1, 0));
	normals->push_back(Vec3(0, -1, 0));

	// Adicionamos as normais rec�m-criadas, e seguindo a conven��o, utilizamos o �nidce 7:
	squareGeometry->setNormalArray(normals);
	squareGeometry->setNormalBinding(Geometry::BIND_PER_VERTEX);
	squareGeometry->setVertexAttribArray(7, normals);
	squareGeometry->setVertexAttribBinding(7, Geometry::BIND_PER_VERTEX);

	// Definimos as coordenadas de textura
	ref_ptr<Vec2Array> texcoords = new Vec2Array(4);
	(*texcoords)[0].set(0.00f, 0.0f);
	(*texcoords)[1].set(1.0f, 0.0f);
	(*texcoords)[2].set(1.0f, 1.0f);
	(*texcoords)[3].set(0.0f, 1.0f);

	// Adicionamos as normais rec�m-criadas, e seguindo a conven��o, utilizamos o �nidce 8:
	squareGeometry->setTexCoordArray(0, texcoords);
	squareGeometry->setVertexAttribArray(8, texcoords);
	squareGeometry->setVertexAttribBinding(8, Geometry::BIND_PER_VERTEX);
}