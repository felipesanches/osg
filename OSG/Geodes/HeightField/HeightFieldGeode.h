#ifndef __HEIGHTFIELDGEODE_H__
#define __HEIGHTFIELDGEODE_H__

#include <osg/Geode>

namespace osgTCC {
	class HeightFieldSurface;

	class HeightFieldGeode : public osg::Geode {
	protected:
		// Armazena a geometria utilizada.
		osg::ref_ptr<osg::Geometry> _heightFieldGeometry;
		// Armazena a superf�cie do heightfield.
		HeightFieldSurface* _heightField;
		// Armazena o espa�amento relativo ao eixo x.
		float _xDistance;
		// Armazena o espa�amento relativo ao eixo y.
		float _yDistance;

	public:
		// Cria um novo HeightfieldGeode a partir do heightmap informado.
		HeightFieldGeode(const std::string& heightMapPath, float heightScale, float xDistance, 
			float yDistance, float radius, float scale, const osg::Vec3 &position = osg::Vec3(0, 0, 0));
		
		// Cria um novo HeightfieldGeode a partir do heightmap informado, utilizando o arquivo
		// de especifica��o
		HeightFieldGeode(const std::string& heightMapPath);

		// Carrega um novo heightmap, substituindo o anterior.
		void loadNewHeightMap(const std::string& heightMapPath, float heightScale, float xDistance, 
			float yDistance, float radius, float scale, const osg::Vec3 &position = osg::Vec3(0, 0, 0));

		// Carrega um novo heightmap, utilizando o arquivo txt como especifica��o. Deve ser passado
		// o caminho para o heightmap. O arquivo de especifica��o deve estar presente junto do mapa.
		void loadNewHeightMap(const std::string& heightMapPath);

		// M�todo respons�vel por modificar a altura em torno da posi��o informada.
		void changeHeight(osg::Vec3 centerPosition, bool down, double elapsedTime);

		// Retorna o primeiro v�rtice do HeightField.
		osg::Vec3 getFirstVertex();

		// Retorna o raio de modifica��o atual.
		float getRadius();
		// Define o novo valor do raio de modifica��o.
		void setRadius(float radius);
		// Retorna o fator de escala atual.
		float getScale();
		// Define o novo valor do fator de escala.
		void setScale(float scale);

	protected:
		// Destrutor protegido
		virtual ~HeightFieldGeode();

		// M�todo respons�vel por adicionar o vetor de posi��es e normais na geometria.
		void addPositionsAndNormals();

		// M�todo respons�vel por definir como os atributos s�o associados na geoemtria.
		void setPositionsAndNormalsBinding();

		// M�todo respons�vel por marcar que os atributos est�o sujos.
		void setDirty();

	};
}
#endif // !__HEIGHTFIELDGEODE_H__