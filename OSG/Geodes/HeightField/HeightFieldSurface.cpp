#include "HeightFieldSurface.h"

#include "HeightFieldVertex.h"
#include "HeightFieldTriangle.h"

#include "../../Utils/Utils.h"

#include <fstream>

using namespace osgTCC;
using namespace osg;
using namespace std;

HeightFieldSurface::HeightFieldSurface(float radius, float scale, unsigned int numberOfRows, unsigned int numberOfColumns,
									   float xDistance, float yDistance) {
	if (radius <= 0 || scale <= 0)
		showErrorMessage("Os par�metros raio e escala devem ser positivos.", 10);
	_radius = radius;
	_scale = scale;
	_numberOfRows = numberOfRows;
	_numberOfColumns = numberOfColumns;
	_xDistance = xDistance;
	_yDistance = yDistance;

	_positions = new Vec3Array();
	_normals = new Vec3Array();
	_positions->setDataVariance(Object::DYNAMIC);
	_normals->setDataVariance(Object::DYNAMIC);
}

HeightFieldSurface::~HeightFieldSurface() {
	// Devemos apagar todos os objetos criados e armazenados
	for (std::vector<HeightFieldTriangle*>::iterator it = _triangles.begin(); it != _triangles.end(); it++)
		delete *it;
	_triangles.clear();
	for (std::vector<HeightFieldVertex*>::iterator it = _vertices.begin(); it != _vertices.end(); it++)
		delete *it;
	_vertices.clear();
}

void HeightFieldSurface::addVertex(Vec3& position) {
	// Devemos adicionar um novo v�rtice
	_vertices.push_back(new HeightFieldVertex(position));
}

void HeightFieldSurface::addTriangle(unsigned int index0, unsigned int index1, unsigned int index2) {
	// Adicionamos um novo tri�ngulo
	_triangles.push_back(new HeightFieldTriangle(_vertices[index0], _vertices[index1], _vertices[index2]));
}

void HeightFieldSurface::updateAllNormals() {
	// Devemos iterar sobre os tri�ngulos, depois sobre os v�rtices
	for (std::vector<HeightFieldTriangle*>::iterator it = _triangles.begin(); it != _triangles.end(); it++)
		(*it)->updateNormal();

	for (std::vector<HeightFieldVertex*>::iterator it = _vertices.begin(); it != _vertices.end(); it++)
		(*it)->updateNormal();

	// Agora limpamos os tri�ngulos
	for (std::vector<HeightFieldTriangle*>::iterator it = _triangles.begin(); it != _triangles.end(); it++)
		(*it)->clean();

	// Podemos agora popular os vetores de posi��es e normais
	for (std::vector<HeightFieldVertex*>::iterator it = _vertices.begin(); it != _vertices.end(); it++) {
		_positions->push_back((*it)->getPosition());
		_normals->push_back((*it)->getNormal());
	}
}

ref_ptr<Vec3Array> HeightFieldSurface::getPositions() {
	return _positions.get();
}

ref_ptr<Vec3Array> HeightFieldSurface::getNormals() {
	return _normals.get();
}

void HeightFieldSurface::changeHeight(osg::Vec3 centerPosition, bool down, double elapsedTime) {
	// Optou-se por modificar a dist�ncia dos pontos, utilizando o quadrado 
	// da diferen�a entre o raio e dist�ncia do centro. Devemos primeiramente
	// reduzir a �rea de procura, pois, em algus casos, teremos um grande n�mero
	// de pontos, o que inviabiliza percorrer todos os v�rtices.
	// Devemos definir os limites desta nova �rea:
	unsigned int firstRow, lastRow;
	unsigned int firstColumn, lastColumn;

	int rowTemp, columnTemp;

	rowTemp = (centerPosition.x() - _vertices[0]->getPosition().x() - _radius) / _xDistance;
	lastRow = (centerPosition.x() - _vertices[0]->getPosition().x() + _radius) / _xDistance;

	columnTemp = (centerPosition.y() - _vertices[0]->getPosition().y() - _radius) / _yDistance;
	lastColumn = (centerPosition.y() - _vertices[0]->getPosition().y() + _radius) / _yDistance;

	// Checamos se as coordenadas s�o v�lidas
	if (rowTemp < 0)
		firstRow = 0;
	else
		firstRow = rowTemp;
	if (lastRow > _numberOfRows)
		lastRow = _numberOfRows;
	if (columnTemp < 0)
		firstColumn = 0;
	else
		firstColumn = columnTemp;
	if (lastColumn > _numberOfColumns)
		lastColumn = _numberOfColumns;

	// Varremos o vetor de v�rtices, utilizando os limites calculados e 
	// modificamos a altura do ponto se necess�rio
	for (unsigned int i = firstRow; i < lastRow; i++) {
		for (unsigned int j = firstColumn; j < lastColumn; j++) {
			float distance = std::sqrtf(std::powf(centerPosition.x() - _vertices[i * _numberOfColumns + j]->getPosition().x(), 2)
				+ std::powf(centerPosition.y() - _vertices[i * _numberOfColumns + j]->getPosition().y(), 2));
			if (distance < _radius) {
				float deltaHeight = std::powf(_radius - distance, 2) * _scale * elapsedTime;
				if (down)
					deltaHeight *= -1;
				_vertices[i * _numberOfColumns + j]->changeHeight(deltaHeight);
				// Atualizamos o vetor de posi��es
				_positions->at(i * _numberOfColumns + j) = _vertices[i * _numberOfColumns + j]->getPosition();
			}
		}
	}

	// Devemos atualizar as normais. Para isso, devemos varrer os tri�ngulos das redondezas.
	if (firstRow > 1)
		firstRow--;
	if (lastRow < _numberOfRows - 3)
		lastRow++;
	else
		lastRow = _numberOfRows - 2;
	if (firstColumn > 1)
		firstColumn--;
	if (lastColumn < _numberOfColumns - 3)
		lastColumn++;
	else
		lastColumn = _numberOfColumns - 2;

	// Atualizamos as normais dos tri�ngulos
	for (unsigned int i = firstRow; i < lastRow; i++) {
		for (unsigned int j = firstColumn; j < lastColumn; j++) {
			_triangles[2 * i * _numberOfColumns + 2 * j]->updateNormal();
			_triangles[2 * i * _numberOfColumns + 2 * j + 1]->updateNormal();
		}
	}

	// Atualizamos as normais dos v�rtices
	for (unsigned int i = firstRow; i < lastRow + 2; i++)
		for (unsigned int j = firstColumn; j < lastColumn + 2; j++) {
			_vertices[i * _numberOfColumns + j]->updateNormal();
			// Atualizamos o vetor de normais
			_normals->at(i * _numberOfColumns + j) = _vertices[i * _numberOfColumns + j]->getNormal();
		}
	
	// Limpamos os tri�ngulos
	for (unsigned int i = firstRow; i < lastRow; i++) {
		for (unsigned int j = firstColumn; j < lastColumn; j++) {
			_triangles[2 * i * _numberOfColumns + 2 * j]->clean();
			_triangles[2 * i * _numberOfColumns + 2 * j + 1]->clean();
		}
	}
}

float HeightFieldSurface::getHighestHeight() {
	// Iteramos na lista de v�rtices procurando o maior z
	float tempZ = -FLT_MAX;
	for (std::vector<HeightFieldVertex*>::iterator it = _vertices.begin(); it != _vertices.end(); it++)
		if (tempZ < (*it)->getPosition().z())
			tempZ = (*it)->getPosition().z();

	return tempZ;
}

float HeightFieldSurface::getLowestHeight() {
	// Iteramos na lista de v�rtices procurando o menor z
	float tempZ = FLT_MAX;
	for (std::vector<HeightFieldVertex*>::iterator it = _vertices.begin(); it != _vertices.end(); it++)
		if (tempZ > (*it)->getPosition().z())
			tempZ = (*it)->getPosition().z();

	return tempZ;
}

osg::Vec3 HeightFieldSurface::getFirtVertex() {
	return _vertices.at(0)->getPosition();
}

float HeightFieldSurface::getRadius() {
	return _radius;
}

void HeightFieldSurface::setRadius(float radius) {
	if (radius <= 0)
		showErrorMessage("Valor invalido de raio de modifica��o", 12);
	_radius = radius;
}

float HeightFieldSurface::getScale() {
	return _scale;
}

void HeightFieldSurface::setScale(float scale) {
	if (scale <= 0)
		showErrorMessage("Valor invalido de fator de escala", 13);
	_scale = scale;
}