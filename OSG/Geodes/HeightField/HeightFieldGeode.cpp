#include "HeightFieldGeode.h"

#include "HeightFieldSurface.h"

#include "../../Utils/Utils.h"

#include <osg/Geometry>
#include <fstream>
#include <iostream>

using namespace osgTCC;
using namespace osg;

HeightFieldGeode::HeightFieldGeode(const std::string& heightMapPath, float heightScale, float xDistance, 
			float yDistance, float radius, float scale, const osg::Vec3 &position) : Geode() {	
	// Para permitir que a instancia��o seja a mesma quando criamos um novo heightfield ou carregamos outro,
	// precisamos, artificialmente adicionar um drawable e heightfieldsurface que ser�o logo em seguida removidos.
	_heightFieldGeometry = new Geometry();
	_heightField = new HeightFieldSurface(radius, scale, 0, 0, xDistance, yDistance);
	addDrawable(_heightFieldGeometry);

	// Agora carregamos o HeightMap
	loadNewHeightMap(heightMapPath, heightScale, xDistance, yDistance, radius, scale, position);
}

HeightFieldGeode::HeightFieldGeode(const std::string& heightMapPath) : Geode() {
	// Para permitir que a instancia��o seja a mesma quando criamos um novo heightfield ou carregamos outro,
	// precisamos, artificialmente adicionar um drawable e heightfieldsurface que ser�o logo em seguida removidos.
	_heightFieldGeometry = new Geometry();
	_heightField = new HeightFieldSurface(1, 1, 1, 1, 1, 1);
	addDrawable(_heightFieldGeometry);

	// Agora carregamos o HeightMap
	loadNewHeightMap(heightMapPath);
}

void HeightFieldGeode::loadNewHeightMap(const std::string& heightMapPath, float heightScale, float xDistance, 
			float yDistance, float radius, float scale, const osg::Vec3 &position) {
	// Checamos a consist�ncia dos argumentos
	if (xDistance <= 0 || yDistance <= 0)
		showErrorMessage("O espacamento entre os pontos do HeightField deve ser positivo.", 8);
	if (heightScale < 0)
		showErrorMessage("O multiplicador de altura do HeightField deve ser positivo.", 8);

	// Logo em seguida, precisamos remover o geometry anterior, e apagar o heightfield surface
	removeDrawable(_heightFieldGeometry);
	delete _heightField;
	
	// Armazenamos os espa�amentos
	_xDistance = xDistance;
	_yDistance = yDistance;

	// Lemos o heightmap
	ref_ptr<Image> heightMap = getImage(heightMapPath);
	
	// Obtemos as dimens�es do heightmap e definimos sua origem
	unsigned int numberOfColumns = heightMap->s();
	unsigned int numberOfRows = heightMap->t();
	
	// Precisamos garantir que o heightmap seja de no m�nimo 2x2
	if ((numberOfColumns < 2) || (numberOfRows < 2))
		showErrorMessage("O heightmap deve possuir no m�nimo 2x2 pontos.", 8);

	Vec3 origin = Vec3(position.x() - numberOfColumns * xDistance / 2.0,
	   position.y() - numberOfRows * yDistance / 2.0, position.z());
	
	// Alocamos um geometry para armazenar o heightfield e o adicionamos ao geode
	_heightFieldGeometry = new Geometry();
	addDrawable(_heightFieldGeometry);

	// Definimos que usaremos VBO e normais
	_heightFieldGeometry->setUseVertexBufferObjects(true);

	// Criamos os v�rtices, varrendo o heightmap e adicionando � nossa superf�cie
	_heightField = new HeightFieldSurface(radius, scale, numberOfRows, numberOfColumns, xDistance, yDistance);
	for (unsigned int i = 0; i < numberOfRows; i++) {
		for (unsigned int j = 0; j < numberOfRows; j++) {
			float x = origin.x() + i * xDistance;
			float y = origin.y() + j * yDistance;
			float z = origin.z() + ((*heightMap->data(j, i)) / 255.0f) * heightScale;
			_heightField->addVertex(Vec3(x, y, z));
		}
	}

	// Definimos como desenharemos os v�rtices, e adicionamos os tri�ngulos na superf�cie
	ref_ptr<DrawElementsUInt> primitiveSet = new DrawElementsUInt(PrimitiveSet::TRIANGLES);
	for (unsigned int i = 1; i < numberOfRows; i++) {
		for (unsigned int j = 0; j < numberOfColumns - 1; j++) {
			// Sempre adicionamos 2 tri�ngulos que formam:
			// * - *
			// | / |
			// * - *
			unsigned int index0 = i * numberOfColumns + j;
			unsigned int index1 = (i - 1) * numberOfColumns + (j + 1);
			unsigned int index2 = (i - 1) * numberOfColumns + j;
			_heightField->addTriangle(index0, index1, index2);
			primitiveSet->push_back(index0);
			primitiveSet->push_back(index1);
			primitiveSet->push_back(index2);

			index0 = i * numberOfColumns + j;
			index1 = i * numberOfColumns + (j + 1);
			index2 = (i - 1) * numberOfColumns + (j + 1);

			_heightField->addTriangle(index0, index1, index2);
			primitiveSet->push_back(index0);
			primitiveSet->push_back(index1);
			primitiveSet->push_back(index2);
		}
	}

	// Adicionamos o primitiveset
	_heightFieldGeometry->addPrimitiveSet(primitiveSet);

	// Devemos primeiramente atualizar as normais
	_heightField->updateAllNormals();

	_heightFieldGeometry->setDataVariance(Object::DYNAMIC);

	// Adicionamos os v�rtices e normais � geometria
	addPositionsAndNormals();

	// Informamos que mudamos os vetores
	setDirty();
}

void HeightFieldGeode::loadNewHeightMap(const std::string& heightMapPath) {
	// Devemos primeiramente obter os par�metros presentes no arquivo de especifica��o
	// Para isso, obtemos o caminho do mapa, sem a extens�o e adicionamos .txt ao fim
	int dotPosition = heightMapPath.find_last_of(".");
	std::string specificationFile = heightMapPath.substr(0, dotPosition) + ".txt";
	
	// Abrimos o arquivo e obtemos os par�metros
	std::ifstream infile;
	infile.open(specificationFile);
	if(!infile.is_open()) {
		showErrorMessage("O arquivo de especifica��o " + specificationFile + " n�o pode ser aberto!", 6);
	}

	float heightScale, xDistance, yDistance, radius, scale;
	infile >> heightScale >> xDistance >> yDistance >> radius >> scale;

	infile.close();

	// Basta carregar o mapa
	loadNewHeightMap(heightMapPath, heightScale, xDistance, yDistance, radius, scale);
}

HeightFieldGeode::~HeightFieldGeode() {
	delete _heightField;
}

void HeightFieldGeode::addPositionsAndNormals() {
	// Adicionamos os vetores de posi��es, normais e slopes
	// Optamos por passar as posi��es, normais e slopes nos �ndices 6, 7 e 8, respectivamente.
	_heightFieldGeometry->setVertexArray(_heightField->getPositions());
	_heightFieldGeometry->setVertexAttribArray(6, _heightField->getPositions());
	_heightFieldGeometry->setNormalArray(_heightField->getNormals());
	_heightFieldGeometry->setVertexAttribArray(7, _heightField->getNormals());
	//_heightFieldGeometry->setVertexArray(_heightField->getSlopes());
	//_heightFieldGeometry->setVertexAttribArray(8, _heightField->getSlopes());
	// Definimos como eles ser�o atribu�dos
	setPositionsAndNormalsBinding();
}

void HeightFieldGeode::setPositionsAndNormalsBinding() {
	_heightFieldGeometry->setVertexAttribBinding(6, Geometry::BIND_PER_VERTEX);
	_heightFieldGeometry->setNormalBinding(Geometry::BIND_PER_VERTEX);
	_heightFieldGeometry->setVertexAttribBinding(7, Geometry::BIND_PER_VERTEX);
	//_heightFieldGeometry->setVertexAttribBinding(8, Geometry::BIND_PER_VERTEX);
}

void HeightFieldGeode::changeHeight(Vec3 centerPosition, bool down, double elapsedTime) {
	_heightField->changeHeight(centerPosition, down, elapsedTime);
	// Setamos como modificados os vetores
	setDirty();
}

void HeightFieldGeode::setDirty() {
	_heightFieldGeometry->getVertexArray()->dirty();
	_heightFieldGeometry->getNormalArray()->dirty();
	_heightFieldGeometry->getVertexAttribArray(6)->dirty();
	_heightFieldGeometry->getVertexAttribArray(7)->dirty();
	_heightFieldGeometry->dirtyBound();//VER DESEMPENHO
}

osg::Vec3 HeightFieldGeode::getFirstVertex() {
	return _heightField->getFirtVertex();
}

float HeightFieldGeode::getRadius() {
	return _heightField->getRadius();
}

void HeightFieldGeode::setRadius(float radius) {
	_heightField->setRadius(radius);
}

float HeightFieldGeode::getScale() {
	return _heightField->getScale();
}

void HeightFieldGeode::setScale(float scale) {
	_heightField->setScale(scale);
}