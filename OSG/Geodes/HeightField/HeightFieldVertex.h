#ifndef __HEIGHTFIELDVERTEX_H__
#define __HEIGHTFIELDVERTEX_H__

#include <osg/Vec3>
#include <set>

namespace osgTCC {
	class HeightFieldTriangle;

	class HeightFieldVertex {
	protected:
		// Armazena a posi��o do v�rtice.
		osg::Vec3 _position;
		// Armazena a informa��o de alguma mudan�a na posi��o.
		bool _dirty;
		// Armazena a normal do v�rtice.
		osg::Vec3 _normal;
		// Armazena o conjunto de tri�ngulos em que o v�rtice participa.
		std::set<HeightFieldTriangle*> _triangles;

	public:
		// Cria um novo HeightfieldVertex com a posi��o informada.
		HeightFieldVertex(osg::Vec3& position);

		// Destrutor padr�o
		virtual ~HeightFieldVertex() { }

		// Define a posi��o do v�rtice.
		void setPosition(osg::Vec3& position) { _position = position; _dirty = true; }

		// Adiciona um novo HeightfieldTriangle ao conjunto de tri�ngulos.
		void addTriangle(HeightFieldTriangle* triangle);

		// Informa se houve alguma mudan�a na posi��o do v�rtice.
		bool isDirty() { return _dirty; }

		// Retorna a posi��o do v�rtice.
		const osg::Vec3 getPosition() { return _position; }

		void changeHeight(float deltaHeight);

		// Calcula a normal se necess�rio, checando mudan�as nos tri�ngulos, al�m de marcar o v�rtice como n�o modificado.
		void updateNormal();

		// Retorna a normal j� calculada do v�rtice.
		const osg::Vec3 getNormal() { return _normal; }

	protected:
		// M�todo respons�vel pelo c�lculo da normal.
		void computeNormal();

	};
}
#endif // !__HEIGHTFIELDVERTEX_H__