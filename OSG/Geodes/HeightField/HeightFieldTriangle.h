#ifndef __HEIGHTFIELDTRIANGLE_H__
#define __HEIGHTFIELDTRIANGLE_H__

#include <osg/Vec3>

namespace osgTCC {
	class HeightFieldVertex;

	class HeightFieldTriangle {
	protected:
		// Armazena o primeiro v�rtice passado.
		HeightFieldVertex* _vertex0;
		// Armazena o segundo v�rtice passado.
		HeightFieldVertex* _vertex1;
		// Armazena o terceiro v�rtice passado.
		HeightFieldVertex* _vertex2;
		// Armazena a normal j� calculada.
		osg::Vec3 _normal;
		// Armazena a informa��o de alguma mudan�a na normal.
		bool _dirty;

	public:
		// Cria um novo HeightfieldTriangle, utilizado para o c�lculo de normais dos v�rtices.
		// Os v�rtices precisam ser passados respeitando o sentido anti-hor�rio.
		// Quando criado, ele se adiciona nos v�rtices passados.
		HeightFieldTriangle(HeightFieldVertex* vertex0, HeightFieldVertex* vertex1, HeightFieldVertex* vertex2);

		// Destrutor padr�o
		virtual ~HeightFieldTriangle() { }

		// Informa se houve alguma mudan�a na normal do tri�ngulo.
		bool isDirty() { return _dirty; }

		// Calcula a normal se necess�rio, checando se houve alguma mudan�a nos v�rtices.
		void updateNormal();

		// Simplesmente retorna a normal calculada e armazenada.
		const osg::Vec3 getNormal() { return _normal; }

		// Simplesmente coloca o indicador de modifica��o em falso.
		void clean() { _dirty = false; }

	protected:
		// M�todo respons�vel pelo c�lculo da normal.
		void computeNormal();

	};
}
#endif // !__HEIGHTFIELDTRIANGLE_H__