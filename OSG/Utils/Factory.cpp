#include "Factory.h"

using namespace osgTCC;

Factory::Factory(EventStatus *eventStauts){
	_eventStatus = eventStauts;
}

void Factory::addEventHandlers(osgViewer::Viewer *v){
	// Criamos um KeyboardHandler
	_kh = new FirstKeyboardHandler(_eventStatus);
	// Associamos o KeyboardHandler ao Viewer
	v->addEventHandler(_kh);

	// Criamos um MouseHandler do terreno
	_mht = new TerrainMouseHandler(_eventStatus);
	// Associamos o MouseHandler ao Viewer
	v->addEventHandler(_mht);

	// Criamos um MouseHandler
	_mh = new FirstMouseHandler(_eventStatus);
	// Associamos o MouseHandler ao Viewer
	v->addEventHandler(_mh);

	// Criamos um CustomStatHandler
	_statsHandler = new CustomStatsHandler(800, 600, _eventStatus);
	v->addEventHandler(_statsHandler);

	// Remover
	// Criamos um StatsHandler
	_sh = new osgViewer::StatsHandler();
	_sh->setKeyEventTogglesOnScreenStats(osgGA::GUIEventAdapter::KEY_F2);
	v->addEventHandler(_sh);
}

bool Factory::setupShaders(){	
	//Criamos o stateSet
	_shaderStateSet = new osg::StateSet();
	
	// Criamos um programa
	_program = new osg::Program();

	// Adicionamos os shaders ao programa
	_program->addShader(getShader(osg::Shader::VERTEX, "../Resources/Shaders/" + _eventStatus->vertShaderName));
	_program->addShader(getShader(osg::Shader::FRAGMENT, "../Resources/Shaders/" + _eventStatus->fragShaderName));

	// Optamos por disponibilizar a posicao e normal 6 e 7, respectivamente.
	_program->addBindAttribLocation("position", 6);
	_program->addBindAttribLocation("normal", 7);

	// Passamos o programa para o stateSet
	_shaderStateSet->setAttributeAndModes(_program, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

	// Invocamos o metodo que ira criar as Uniforms que serao usadas
	setupUniforms();

	// Invocamos o metodo que ira carregar as texturas a serem usadas
	loadTextures(16.0f);

	// Associamos o stateSet a um n�
	osg::Node* heightfield = findNamedNode("HeightField", _root);
	heightfield->asGeode()->setStateSet(_shaderStateSet);

	return true;
}

void Factory::setupUniforms(){
	// Criamos as uniforms das matrix
	osg::ref_ptr<osg::Uniform> wMatrix = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "worldMatrix");
	osg::ref_ptr<osg::Uniform> vMatrix = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "viewMatrix");
	osg::ref_ptr<osg::Uniform> pMatrix = new osg::Uniform(osg::Uniform::FLOAT_MAT4, "projectionMatrix");
	// O primeiro ponto do heightmap
	osg::ref_ptr<osg::Uniform> fVertex = new osg::Uniform("firstVertex", _heightField->getFirstVertex());
	// Samplers usados
	osg::ref_ptr<osg::Uniform> sandSampler = new osg::Uniform("sandSampler", 0);
	osg::ref_ptr<osg::Uniform> grassSampler = new osg::Uniform("grassSampler", 1);
	osg::ref_ptr<osg::Uniform> rockSampler = new osg::Uniform("rockSampler", 2);
	osg::ref_ptr<osg::Uniform> snowSampler = new osg::Uniform("snowSampler", 3);
	osg::ref_ptr<osg::Uniform> noiseSampler = new osg::Uniform("noiseSampler", 4);
	// Uniform para materiais
	osg::ref_ptr<osg::Uniform> sandMat = new osg::Uniform("materials.Sand", osg::Vec4f(0.8f,0.8f,0.8f,0.8f));
	osg::ref_ptr<osg::Uniform> grassMat = new osg::Uniform("materials.Grass", osg::Vec4f(1,1,0,0.3f));
	osg::ref_ptr<osg::Uniform> rockMat = new osg::Uniform("materials.Rock", osg::Vec4f(0.6f,0.6f,0.6f,1.0f));
	osg::ref_ptr<osg::Uniform> snowMat = new osg::Uniform("materials.Snow", osg::Vec4f(1,1,1,0.6f));
	// Uniform para Luz
	osg::ref_ptr<osg::Uniform> lightColor = new osg::Uniform("light.Color", osg::Vec3(1,1,1));
	osg::ref_ptr<osg::Uniform> lightAmbIntensity = new osg::Uniform("light.AmbientIntensity", 0.5f);
	osg::ref_ptr<osg::Uniform> lightDirection = new osg::Uniform("light.Direction", osg::Vec3(1,0,-0.10));
	osg::ref_ptr<osg::Uniform> lightDiffuseIntensity = new osg::Uniform("light.DiffuseIntensity", 0.75f);


	// Criamos e setamos os callbacks que atualizaram as uniforms usadas
	osg::ref_ptr<WorldMatrixUniformCallback> wMatrixCallback = new WorldMatrixUniformCallback();
	osg::ref_ptr<ViewMatrixUniformCallback> vMatrixCallback = new ViewMatrixUniformCallback(_camera);
	osg::ref_ptr<ProjectionMatrixUniformCallback> pMatrixCallback = new ProjectionMatrixUniformCallback(_camera);
	wMatrix->setUpdateCallback(wMatrixCallback);
	vMatrix->setUpdateCallback(vMatrixCallback);
	pMatrix->setUpdateCallback(pMatrixCallback);

	// Associamos as uniforms ao stateset
	_shaderStateSet->addUniform(wMatrix);
	_shaderStateSet->addUniform(vMatrix);
	_shaderStateSet->addUniform(pMatrix);
	_shaderStateSet->addUniform(fVertex);
	_shaderStateSet->addUniform(sandSampler);
	_shaderStateSet->addUniform(grassSampler);
	_shaderStateSet->addUniform(rockSampler);
	_shaderStateSet->addUniform(snowSampler);
	_shaderStateSet->addUniform(noiseSampler);
	_shaderStateSet->addUniform(sandMat);
	_shaderStateSet->addUniform(grassMat);
	_shaderStateSet->addUniform(rockMat);
	_shaderStateSet->addUniform(snowMat);
	_shaderStateSet->addUniform(lightColor);
	_shaderStateSet->addUniform(lightAmbIntensity);
	_shaderStateSet->addUniform(lightDirection);
	_shaderStateSet->addUniform(lightDiffuseIntensity);
	
}

void Factory::loadTextures(float filteringLevel){
	// Criamos a textura para a areia
	osg::ref_ptr<osg::Texture2D> sandTex = new osg::Texture2D();

	// Configuramos e carregamos a imagem desejada
	sandTex->setDataVariance(osg::Object::DYNAMIC);
	sandTex->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	sandTex->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
	sandTex->setImage(getImage("../Resources/Textures/TerrainBase/sandv2.tga"));
	sandTex->setMaxAnisotropy(filteringLevel);

	// Indicamos as texturas para o stateSet
	_shaderStateSet->setTextureAttributeAndModes(0, sandTex, osg::StateAttribute::ON);


	// Criamos a textura para a grama
	osg::ref_ptr<osg::Texture2D> grassTex = new osg::Texture2D();

	// Configuramos e carregamos a imagem desejada
	grassTex->setDataVariance(osg::Object::DYNAMIC);
	grassTex->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	grassTex->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
	grassTex->setImage(getImage("../Resources/Textures/TerrainBase/grassv2.tga"));
	grassTex->setMaxAnisotropy(filteringLevel);

	// Indicamos as texturas para o stateSet
	_shaderStateSet->setTextureAttributeAndModes(1, grassTex, osg::StateAttribute::ON);



	// Criamos a textura para a pedra
	osg::ref_ptr<osg::Texture2D> rockTex = new osg::Texture2D();

	// Configuramos e carregamos a imagem desejada
	rockTex->setDataVariance(osg::Object::DYNAMIC);
	rockTex->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	rockTex->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
	rockTex->setImage(getImage("../Resources/Textures/TerrainBase/rockv3.tga"));
	rockTex->setMaxAnisotropy(filteringLevel);

	// Indicamos as texturas para o stateSet
	_shaderStateSet->setTextureAttributeAndModes(2, rockTex, osg::StateAttribute::ON);


	// Criamos a textura para a neve
	osg::ref_ptr<osg::Texture2D> snowTex = new osg::Texture2D();

	// Configuramos e carregamos a imagem desejada
	snowTex->setDataVariance(osg::Object::DYNAMIC);
	snowTex->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	snowTex->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
	snowTex->setImage(getImage("../Resources/Textures/TerrainBase/snowv2.tga"));
	snowTex->setMaxAnisotropy(filteringLevel);

	// Indicamos as texturas para o stateSet
	_shaderStateSet->setTextureAttributeAndModes(3, snowTex, osg::StateAttribute::ON);

	
	// Criamos a textura para o ruido de pperlin
	osg::ref_ptr<osg::Texture2D> perlinTex = new osg::Texture2D();

	// Configuramos e carregamos a imagem desejada
	perlinTex->setDataVariance(osg::Object::DYNAMIC);
	perlinTex->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
	perlinTex->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
	perlinTex->setImage(getImage("../Resources/Textures/Noise/perlinnoise2.png"));
	perlinTex->setMaxAnisotropy(filteringLevel);

	// Indicamos as texturas para o stateSet
	_shaderStateSet->setTextureAttributeAndModes(4, perlinTex, osg::StateAttribute::ON);
	
}

void Factory::setupCamera(osgViewer::Viewer* v, osg::Vec3f initPos, osg::Vec3f mainTargetPos,
									int windowWidth, int windowHeight){
	FreeCameraManipulator* manipulator = new FreeCameraManipulator(initPos, _eventStatus);
	v->setCameraManipulator(manipulator);

	// Guarda um referencia para a camera usada no viewer
	_camera = v->getCamera();

	//Modifica a mascara da camera
	_camera->setCullMask(0x00000001);
}

osg::Group* Factory::initializeGraph(){
	// Criamos a raiz do grafo
	_root = new osg::Group();
	// Associamos um callback a raiz, junto com o status
	osg::ref_ptr<RootUpdateCallback> rCallback = new RootUpdateCallback(_eventStatus);
	_root->setUpdateCallback(rCallback);


	// Adicionamos um heightmap
	//_heightField = new HeightFieldGeode("../Resources/HeightMaps/" + _eventStatus->heightmapName, 30, 1, 1, 50, 0.01f);
	_heightField = new HeightFieldGeode("../Resources/HeightMaps/" + _eventStatus->heightmapName);
	_heightField->setName("HeightField");
	_root->addChild(_heightField);
	osg::ref_ptr<HeightmapUpdateCallback> hmCallback = new HeightmapUpdateCallback(_eventStatus, this);
	_heightField->setUpdateCallback(hmCallback);
	
	// Adicionamos uma HUD para editar o mapa
	osg::ref_ptr<EditorHUD> editorHUD = new EditorHUD(800, 600, _eventStatus->aoe);
	_root->addChild(editorHUD);
	// Associamos um callback para realizar os updates dessa classe
	osg::ref_ptr<EditorHUDCallback> editorCallback = new EditorHUDCallback(_eventStatus);
	editorHUD->setUpdateCallback(editorCallback);

	
	// Adicionamos um HUD para input de arquivos
	osg::ref_ptr<InputHUD> inputHUD = new InputHUD(800, 600, _eventStatus->vertShaderName.c_str(), 
										_eventStatus->fragShaderName.c_str(), _eventStatus->heightmapName.c_str());
	_root->addChild(inputHUD);
	// Associamos um callback para realizar os updates dessa classe
	osg::ref_ptr<InputHUDCallback> hudCallback = new InputHUDCallback(_eventStatus);
	inputHUD->setUpdateCallback(hudCallback);

	_root->addChild(_statsHandler->getStatsHUD());
	
	return _root.get();
}
