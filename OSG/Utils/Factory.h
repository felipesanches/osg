#ifndef FACTORY_H
#define FACTORY_H

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include "../EventHandlers/Mouse/TerrainMouseHandler.h"
#include "../EventHandlers/Mouse/FirstMouseHandler.h"
#include "../EventHandlers/Keyboard/FirstKeyboardHandler.h"

#include "../UserDatas/EventStatus.h"

#include "../HUD/Interfaces/InputHUD.h"
#include "../HUD/Interfaces/EditorHUD.h"

#include "../Callbacks/Nodes/Update/RootUpdateCallback.h"
#include "../Callbacks/Nodes/Update/HeightmapUpdateCallback.h"
#include "../Callbacks/HUD/InputHUDCallback.h"
#include "../Callbacks/HUD/EditorHUDCallback.h"
#include "../Callbacks/Uniforms/Matrices/ProjectionMatrixUniformCallback.h"
#include "../Callbacks/Uniforms/Matrices/ViewMatrixUniformCallback.h"
#include "../Callbacks/Uniforms/Matrices/WorldMatrixUniformCallback.h"

#include "../Geodes/CubeGeode.h"
#include "../Geodes/SquareGeode.h"
#include "../Geodes/HeightField/HeightFieldGeode.h"

#include "../Camera/FreeCameraManipulator.h"

#include "../EventHandlers/Stats/CustomStatsHandler.h"

#include "Utils.h"

#include <osgViewer/ViewerEventHandlers>

namespace osgTCC {
	class Factory {
	public:
		Factory(EventStatus *eventStauts);
		void addEventHandlers(osgViewer::Viewer *v);
		bool setupShaders();
		osg::Group* initializeGraph();
		void setupCamera(osgViewer::Viewer* v, osg::Vec3f initPos, 
								osg::Vec3f mainTargetPos, int windowWidth, int windowHeight);

	protected:
		virtual ~Factory() {}

	private:
		osg::ref_ptr<EventStatus> _eventStatus;
		osg::ref_ptr<FirstKeyboardHandler> _kh;
		osg::ref_ptr<TerrainMouseHandler> _mht;
		osg::ref_ptr<FirstMouseHandler> _mh;
		osg::ref_ptr<CustomStatsHandler> _statsHandler;
		osg::ref_ptr<osg::Group> _root;
		osg::ref_ptr<HeightFieldGeode> _heightField;
		osg::ref_ptr<osg::Program> _program;
		osg::ref_ptr<osg::StateSet> _shaderStateSet;
		osg::Camera* _camera;

		//Remover
		osg::ref_ptr<osgViewer::StatsHandler> _sh;

		void setupUniforms();
		void loadTextures(float filteringLevel = 1.0f);
	};
}
#endif /* FACTORY_H */