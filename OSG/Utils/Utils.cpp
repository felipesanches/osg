#include "Utils.h"

#include <osgDB/ReadFile>
#include <osg/PolygonMode>

using namespace osgTCC;

void osgTCC::showErrorMessage(const std::string &message, int error) {
	std::cerr << message << std::endl;
	system("PAUSE");
	exit(error);
}

osg::ref_ptr<osg::Image> osgTCC::getImage(const std::string &fileName) {
	// Carregamos a imagem
	osg::ref_ptr<osg::Image> image = osgDB::readImageFile(fileName);
	// Checamos se carregamos corretamente
	if (image == NULL)
		showErrorMessage(
		"A imagem do arquivo \"" + fileName + "\" nao foi carregada.", 1);
	// Retornamos a imagem
	return image;
}

osg::ref_ptr<osg::Shader> osgTCC::getShader(osg::Shader::Type type, const std::string &fileName) {
	// Carregamos o shader
	osg::ref_ptr<osg::Shader> shader = osgDB::readShaderFile(type, fileName);
	// Checamos se carregamos corretamente
	if (shader == NULL)
		showErrorMessage(
		"O shader do arquivo \"" + fileName + "\" nao foi carregada.", 1);
	// Retornamos o shader
	return shader;
}

void osgTCC::wireFrameMode(osg::Node *node, bool on) {
	// Checamos se o n� n�o � nulo
	if (node == NULL)
		showErrorMessage("O node informado e nulo.", 2);
	// Obtemos o StateSet atual do n�, ou criamos um
	osg::ref_ptr<osg::StateSet> state = node->getOrCreateStateSet();

	// Devemos obter o PolygonMode do StateSet se existir
	osg::ref_ptr<osg::PolygonMode> polyModeObj = dynamic_cast<osg::PolygonMode *>(
		state->getAttribute(osg::StateAttribute::POLYGONMODE));

	// Checamos se o PolygonMode � nulo, se sim, adicionamos um novo ao estado
	if (!polyModeObj) {
		polyModeObj = new osg::PolygonMode;
		state->setAttribute(polyModeObj);
	}

	// Agora podemos definir o modo wireframe como ligado ou n�o
	if (on)
		polyModeObj->setMode(osg::PolygonMode::FRONT_AND_BACK,
		osg::PolygonMode::LINE);
	else
		polyModeObj->setMode(osg::PolygonMode::FRONT_AND_BACK,
		osg::PolygonMode::FILL);
}

void osgTCC::textureMode(osg::Node *node, bool on) {
	// Checamos se o n� n�o � nulo
	if (node == NULL)
		showErrorMessage("O node informado e nulo.", 3);
	// Obtemos o StateSet atual do n�, ou criamos um
	osg::ref_ptr<osg::StateSet> state = node->getOrCreateStateSet();

	// Agora podemos definir o modo textura como ligado ou n�o
	if (on)
		state->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::PROTECTED |
		osg::StateAttribute::ON);
	else
		state->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::PROTECTED |
		osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
}

void osgTCC::lightingMode(osg::Node *node, bool on) {
	// Checamos se o n� n�o � nulo
	if (node == NULL)
		showErrorMessage("O node informado e nulo.", 4);
	// Obtemos o StateSet atual do n�, ou criamos um
	osg::ref_ptr<osg::StateSet> state = node->getOrCreateStateSet();

	// Agora podemos definir o modo textura como ligado ou n�o
	if (on)
		state->setMode(GL_LIGHTING,
		osg::StateAttribute::PROTECTED | osg::StateAttribute::ON);
	else
		state->setMode(GL_LIGHTING,
		osg::StateAttribute::PROTECTED | osg::StateAttribute::OFF);
}

osg::Node* osgTCC::findNamedNode(const std::string& searchName, osg::Node* currNode){
	osg::Group* currGroup;
	osg::Node* foundNode;

	// Checa se o node passado nao � nulo
	if(!currNode){
		return NULL;
	}

	// Checa se o node atual � o no procurado
	if(currNode->getName() == searchName){
		return currNode;
	}

	currGroup = currNode->asGroup();
	//Vamos procurar em cada um dos filhos deste n�
	//Para isso faz uma chamada recursiva deste metodo
	if(currGroup){
		for(unsigned int i = 0; i < currGroup->getNumChildren(); i++){
			foundNode = findNamedNode(searchName, currGroup->getChild(i));
			if(foundNode){
				return foundNode;
			}
			return NULL;
		}
	}
	else{
		return NULL;
	}
}