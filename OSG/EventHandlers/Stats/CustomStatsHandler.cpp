#include "CustomStatsHandler.h"
#include <iostream>

using namespace osgTCC;
using namespace osg;
using namespace osgGA;
using namespace osgViewer;

CustomStatsHandler::CustomStatsHandler(double windowWidth, double windowHeight, EventStatus *eventStatus,
									   osgGA::GUIEventAdapter::KeySymbol toggleKey) : GUIEventHandler() {
	// Marcamos que a tecla de toggle n�o foi pressionada e salvamos a tecla escolhida
	_keyIsDown = false;
	_toggleKey = toggleKey;

	// Criamos a HUD
	_statsHUD = new StatsHUD(windowWidth, windowHeight, eventStatus);
}

bool CustomStatsHandler::handle(const GUIEventAdapter &ea,
								GUIActionAdapter &aa, Object *,
								NodeVisitor *) {
	// Checamos se a tecla F1 � a capturada
	if (ea.getKey() != _toggleKey)
		return false;
	// Checamos se a tecla continua pressionada
	if (_keyIsDown && ea.getEventType() == GUIEventAdapter::KEYDOWN)
		return true;
	// Se atingimos essa �rea, devemos habilitar ou desabilitar a HUD
	// Atualizamos o status da tecla F1
	if (ea.getEventType() == GUIEventAdapter::KEYDOWN) {
		_keyIsDown = true;
		// Realizamos o toggle para mostrar ou ocultar o status
		_statsHUD->toggle();
	} else
		_keyIsDown = false;

	return true;
}

osg::ref_ptr<StatsHUD> CustomStatsHandler::getStatsHUD() {
	return _statsHUD;
}
