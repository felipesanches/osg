#ifndef __CUSTOMSTATSHANDLER_H__
#define __CUSTOMSTATSHANDLER_H__

#include <osgGA/GUIEventHandler>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>
#include <osgViewer/Viewer>

#include "../../HUD/Others/StatsHUD.h"

namespace osgTCC {
	class EventStatus;
	
	class CustomStatsHandler : public osgGA::GUIEventHandler {
	public:
		CustomStatsHandler(double windowWidth, double windowHeight, EventStatus *eventStatus,
			osgGA::GUIEventAdapter::KeySymbol toggleKey = osgGA::GUIEventAdapter::KEY_F1);
		
		virtual bool handle(const osgGA::GUIEventAdapter &ea,
			osgGA::GUIActionAdapter &aa, osg::Object *,
			osg::NodeVisitor *);
		
		osg::ref_ptr<StatsHUD> getStatsHUD();

	protected:
		// Destrutor protegido
		virtual ~CustomStatsHandler() {}

		osg::ref_ptr<StatsHUD> _statsHUD;

		// Armazena se a tecla de toggle est� pressionada
		bool _keyIsDown;

		// Armazena a tecla de toggle
		osgGA::GUIEventAdapter::KeySymbol _toggleKey;

	};
}
#endif // !__CUSTOMSTATSHANDLER_H__