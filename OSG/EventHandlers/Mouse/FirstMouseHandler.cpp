#include "FirstMouseHandler.h"
#include "iostream"

using namespace osgTCC;
using namespace std;

FirstMouseHandler::FirstMouseHandler(EventStatus *eventStatus) {
	_eventStatus = eventStatus;
	_eventStatus->mouseX = _eventStatus->windowWidth/2;
	_eventStatus->mouseY = _eventStatus->windowHeight/2;
}
bool FirstMouseHandler::handle(const osgGA::GUIEventAdapter &ea,
			osgGA::GUIActionAdapter &aa, osg::Object *,
			osg::NodeVisitor *nv) {
	if(!_eventStatus->wrapMouseEvent){
		_eventStatus->mouseX = ea.getX();
		_eventStatus->mouseY = ea.getY();
		if(_eventStatus->freeMouseToggle == MFM_OFF){
			aa.requestWarpPointer(floor(_eventStatus->windowWidth/2), floor(_eventStatus->windowHeight/2));
			_eventStatus->wrapMouseEvent = true;
		}
		else if(_eventStatus->freeMouseToggle == MFM_TRANSITION){
			aa.requestWarpPointer(floor(_eventStatus->windowWidth/2), floor(_eventStatus->windowHeight/2));
			_eventStatus->mouseX = floor(_eventStatus->windowWidth/2);
			_eventStatus->mouseY = floor(_eventStatus->windowHeight/2);
			_eventStatus->wrapMouseEvent = true;
			_eventStatus->freeMouseToggle = MFM_OFF;
		}

		//Informa o evento de clique
		if(ea.getButtonMask()&osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON){
			_eventStatus->mouseLeftB = true;
		}
		else{
			_eventStatus->mouseLeftB = false;
		}

		//Informa as novas dimensoes na ocorrencia do evento de resize
		if(ea.getEventType() == osgGA::GUIEventAdapter::RESIZE){
			_eventStatus->windowWidth = ea.getWindowWidth();
			_eventStatus->windowHeight = ea.getWindowHeight();
			_eventStatus->resizeEvent = true;
		}
		else if(!osgGA::GUIEventAdapter::MOVE){
			_eventStatus->resizeEvent = false;
		}

		return true;
	}
	else{
		//Se chegou aqui � porque o evento que chamou o handler foi o wrap feito no mouse
		_eventStatus->wrapMouseEvent = false;
		return false;
	}
}