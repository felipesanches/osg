#ifndef __TERRAINMOUSEHANDLER_H__
#define __TERRAINMOUSEHANDLER_H__

#include <osgGA/GUIEventHandler>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>


#include "../../Geodes/HeightField/HeightFieldGeode.h"

#include "../../UserDatas/EventStatus.h"

#include <osgViewer/Viewer>
#include <osgUtil/LineSegmentIntersector>

namespace osgTCC {
	class TerrainMouseHandler : public osgGA::GUIEventHandler {
	public:
		TerrainMouseHandler(EventStatus *eventStatus);
		virtual bool handle(const osgGA::GUIEventAdapter &ea,
			osgGA::GUIActionAdapter &aa, osg::Object *,
			osg::NodeVisitor *);

	protected:
		// Destrutor protegido.
		virtual ~TerrainMouseHandler() {}

		// Armazena a informa��o de que o bot�o esquerdo est� pressionado.
		bool leftButtonIsPressed;
		// Armazena o tempo da �ltima chaamda da fun��o handle.
		double lastTime;

		EventStatus *_eventStatus;

	};
}
#endif // !__TERRAINMOUSEHANDLER_H__