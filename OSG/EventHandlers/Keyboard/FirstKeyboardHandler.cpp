#include "FirstKeyboardHandler.h"
#include "../../Viewers/WindowedViewer.h"
#include "iostream"

using namespace osgGA;
using namespace osgTCC;
using namespace std;

FirstKeyboardHandler::FirstKeyboardHandler(EventStatus *eventStatus)
    : GUIEventHandler() {
  this->eventStatus = eventStatus;

  ksh = new KeyboardStatsHandler();
}

bool FirstKeyboardHandler::handle(const GUIEventAdapter &ea,
                                  GUIActionAdapter &aa, osg::Object *,
                                  osg::NodeVisitor *) {

	if(ksh->checkKeyPressed(ea.getKey(), ea.getEventType())){
		// Estamos trocando algum status
		eventStatus->dirty = true;

		// Armazenamos a tecla preciosanda
		if(!ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_SHIFT){
			eventStatus->keyboardKeyValue = ea.getKey();
			eventStatus->keyRead = false;
		}
		else{
			eventStatus->keyboardKeyValue = ea.getKey() - 32;
			eventStatus->keyRead = false;
		}

		// Decidimos quem
		switch (ea.getKey()) {
		case GUIEventAdapter::KEY_W:
			if(ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_ALT){
				if(eventStatus->wireframe)
					eventStatus->wireframe = false;
				else
					eventStatus->wireframe = true;
			}
			else{
				eventStatus->camMovimentDirection = CAMERA_FORWARD;
			}
			break;
		case GUIEventAdapter::KEY_L:
			if(ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_ALT){
				if(eventStatus->lighting)
					eventStatus->lighting = false;
				else
					eventStatus->lighting = true;
			}
			break;
		case GUIEventAdapter::KEY_T:
			if(ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_ALT){
				if(eventStatus->texture)
					eventStatus->texture = false;
				else
					eventStatus->texture = true;
			}
			break;
		case GUIEventAdapter::KEY_I:
			if(ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_ALT){
				if(eventStatus->inputHudStatus == HUD_INUSE)
					eventStatus->inputHudStatus = HUD_CLOSE;
				else if(eventStatus->inputHudStatus == HUD_NOP)
					eventStatus->inputHudStatus = HUD_OPEN;
			}
			break;
		case GUIEventAdapter::KEY_B:
			if(ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_ALT){
				if(eventStatus->editorHudStatus == HUD_INUSE)
					eventStatus->editorHudStatus = HUD_CLOSE;
				else if(eventStatus->editorHudStatus == HUD_NOP)
					eventStatus->editorHudStatus = HUD_OPEN;
			}
			break;
		case GUIEventAdapter::KEY_9:
			if(eventStatus->editorHudStatus == HUD_INUSE){
				eventStatus->AOEinfo = INCREASE_AOE;
			}
			break;
		case GUIEventAdapter::KEY_0:
			if(eventStatus->editorHudStatus == HUD_INUSE){
				eventStatus->AOEinfo = DECREASE_AOE;
			}
			break;
		case GUIEventAdapter::KEY_S:
			eventStatus->camMovimentDirection = CAMERA_BACKWARD;
			break;
		case GUIEventAdapter::KEY_A:
			eventStatus->camMovimentDirection = CAMERA_LEFT;
			break;
		case GUIEventAdapter::KEY_D:
			eventStatus->camMovimentDirection = CAMERA_RIGHT;
			break;
		case GUIEventAdapter::KEY_Q:
			eventStatus->camMovimentDirection = CAMERA_UP;
			break;
		case GUIEventAdapter::KEY_E:
			eventStatus->camMovimentDirection = CAMERA_DOWN;
			break;
		case GUIEventAdapter::KEY_F:
			if(ea.getModKeyMask()&GUIEventAdapter::MODKEY_LEFT_ALT){
				if(eventStatus->freeMouseToggle == MFM_ON){
					eventStatus->freeMouseToggle = MFM_TRANSITION;
				}
				else if(eventStatus->freeMouseToggle == MFM_OFF){
					eventStatus->freeMouseToggle = MFM_ON;
				}
			}
			break;
		default:
			eventStatus->camMovimentDirection = CAMERA_NOP;
			break;
		}
		return true;
	}

	//Tratamos aqui o caso de teclas que se mantiveram precionadas
	else if(ksh->checkKeyContinuesPressed(ea.getKey())){
		switch (ea.getKey()) {
		case GUIEventAdapter::KEY_W:
			eventStatus->camMovimentDirection = CAMERA_FORWARD;
			break;
		case GUIEventAdapter::KEY_S:
			eventStatus->camMovimentDirection = CAMERA_BACKWARD;
			break;
		case GUIEventAdapter::KEY_A:
			eventStatus->camMovimentDirection = CAMERA_LEFT;
			break;
		case GUIEventAdapter::KEY_D:
			eventStatus->camMovimentDirection = CAMERA_RIGHT;
			break;
		case GUIEventAdapter::KEY_Q:
			eventStatus->camMovimentDirection = CAMERA_UP;
			break;
		case GUIEventAdapter::KEY_E:
			eventStatus->camMovimentDirection = CAMERA_DOWN;
			break;
		}
		return true;
	}
	return false;
}