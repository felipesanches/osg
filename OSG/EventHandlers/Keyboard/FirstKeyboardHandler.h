#ifndef __FIRSTKEYBOARDHANDLER_H__
#define __FIRSTKEYBOARDHANDLER_H__

#include <osgGA/GUIEventHandler>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>

#include "../../UserDatas/EventStatus.h"

#include "KeyboardStatsHandler.h"

namespace osgTCC {
	class FirstKeyboardHandler : public osgGA::GUIEventHandler {
	public:
		FirstKeyboardHandler(EventStatus *eventStatus);
		virtual bool handle(const osgGA::GUIEventAdapter &ea,
			osgGA::GUIActionAdapter &aa, osg::Object *,
			osg::NodeVisitor *);

	protected:
		// Destrutor protegido
		virtual ~FirstKeyboardHandler() {}

		// Armazenamos o objeto com o status
		osg::ref_ptr<EventStatus> eventStatus;
		KeyboardStatsHandler* ksh;
	};
}
#endif // !__FIRSTKEYBOARDHANDLER_H__