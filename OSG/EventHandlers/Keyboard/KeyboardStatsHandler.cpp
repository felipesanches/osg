#include "KeyboardStatsHandler.h"

using namespace osgTCC;
using namespace osgGA;

KeyboardStatsHandler::KeyboardStatsHandler(){
  // Marcamos as teclas que ser�o utilizadas;
  keyStatusMap[GUIEventAdapter::KEY_Q] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_W] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_E] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_R] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_T] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Y] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_U] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_I] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_O] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_P] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_A] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_S] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_D] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_F] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_G] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_H] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_J] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_K] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_L] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Z] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_X] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_C] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_V] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_B] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_N] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_M] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_0] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_1] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_2] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_3] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_4] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_5] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_6] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_7] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_8] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_9] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Colon] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Comma] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_BackSpace] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Semicolon] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Backslash] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Slash] = GUIEventAdapter::KEYUP;
  keyStatusMap[GUIEventAdapter::KEY_Period] = GUIEventAdapter::KEYUP;
}

bool KeyboardStatsHandler::checkKeyPressed(int key, GUIEventAdapter::EventType eventType){
	// Checamos se a tecla � uma das dispon�veis
	if(keyStatusMap.end() == keyStatusMap.find(key))
		return false;

	// Checamos se a tecla est� sendo pressionada
	if (eventType == GUIEventAdapter::KEYDOWN){
		// Como estamos nos preocupando apenas com o pressionamento, checamos se a tecla estava solta antes
		if (keyStatusMap[key] == GUIEventAdapter::KEYUP){
			// Trocamos o status
			keyStatusMap[key] = GUIEventAdapter::KEYDOWN;
			return true;
		}
	}
	else if(eventType == GUIEventAdapter::KEYUP){
		keyStatusMap[key] = GUIEventAdapter::KEYUP;
	}
	return false;
}

bool KeyboardStatsHandler::checkKeyContinuesPressed(int key){
	// Checamos se a tecla � uma das dispon�veis
	if(keyStatusMap.end() == keyStatusMap.find(key))
		return false;

	if (keyStatusMap[key] == GUIEventAdapter::KEYDOWN){
		return true;
	}
	return false;
}

