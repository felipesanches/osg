#ifndef KEYBOARDSTATSHANDLER_H
#define KEYBOARDSTATSHANDLER_H

#include <osgGA/GUIEventHandler>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>

namespace osgTCC {
	class KeyboardStatsHandler {
	public:
		KeyboardStatsHandler();
		bool checkKeyPressed(int key, osgGA::GUIEventAdapter::EventType eventType);
		bool checkKeyContinuesPressed(int key);

	protected:
		// Destrutor protegido
		virtual ~KeyboardStatsHandler() {}

		typedef std::map<int, osgGA::GUIEventAdapter::EventType> KeyStatusMap;
		KeyStatusMap keyStatusMap;
	};
}
#endif /* KEYBOARDSTATSHANDLER_H */